function [x, fVal, exitFlag] = solveLinearProgram(f, A, b, Aeq, beq, lb, ub)
% SOLVELINEARPROGRAM Solve a linear program in the form:
% 
%   maximize:      f'*x
%   subject to:     A*x <= b,
%                 Aeq*x == beq,
%               lb <= x <= ub.
%
% Currently, only gurobi is supported. Other solvers can be wrapped inside
% this function.
% 
% Exit flags:
%   1:  Success
%   0:  No solution found

%  Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
%  Computational Systems Biology group, ETH Z�rich
%
%  This software is freely available under the GNU General Public License v3.
%  See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    %% Build gurobi model
    nDimensions = size(A, 2);
    model.obj = f;
    model.modelsense='max';

    if nargin < 4
        model.A = sparse(A);
        model.rhs = b;
        model.sense = repmat('<',length(b),1);
    else
        model.A = sparse([A; Aeq]);
        model.rhs = [b; beq];
        model.sense = [repmat('<', size(A,1), 1); repmat('=', size(Aeq,1), 1)];
    end

    if nargin < 6
        model.lb = -Inf(nDimensions, 1);
    else
        model.lb = lb;
    end

    if nargin < 7
       model.ub = Inf(nDimensions, 1);
    else
       model.ub = ub;
    end
    
    %% Solve model.
    params.outputflag = 0;
    params.LogToConsole = 1;
    params.FeasibilityTol=10^(-9);
    params.OptimalityTol=10^(-6);
    result = gurobi(model, params);

    %% Process result.
    if strcmp(result.status, 'OPTIMAL')
        exitFlag = 1;
    else 
        exitFlag = 0;
    end
    
    if isfield(result, 'x')
        x = result.x;
    else
        x = nan(nDimensions,1);
    end

    if isfield(result, 'objval')
        fVal = -result.objval;
    else
        fVal = nan;
    end
end
    