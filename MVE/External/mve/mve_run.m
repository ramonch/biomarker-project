function [f,E] = mve_run(A,b,x0)
%  MVE_RUN  Find the maximum volume ellipsoid of a polytope.
%
%     [f,E] = MVE_RUN(A, b) finds the maximum volume inscribed ellipsoid of
%     the polytope {x: A*x <= b}. The polytope is assumed to be full-
%     volume. The ellipse is returned as the pair (f,E) such that
%         Ell = {E^(-1)*x + f: norm(x) <= 1}
%     or equivalently as
%         Ell = {x:  norm(E*(x-f)) <= 1},
%     The matrix E is upper triangular.
%
%     NOTE: An ellipsoid can be represented as:
%     (1) {x: (x-f)'*(E'*E)*(x-f) <= 1}  , or
%     (2) {x: {E^(-1)*x + f, norm(x) <= 1}
%     
%
%     [f,E] = MVE_RUN(A, b, x0) finds the maximum volume inscribed
%     ellipsoid using the starting point x0. It must be an interior point,
%     i.e. b - A*x0 > 0.

%--------------------------------------
% Yin Zhang, Rice University, 07/29/02
%--------------------------------------
% Markus Uhr, ETH Zurich, 10/04/11
%--------------------------------------

maxiter = 200;%1000;%80;

%tol1 = 1.e-7; % presolve tol
%tol2 = 1.e-6; % solver tol
tol1 = 1.e-7;
tol2 = 1.e-8;

t0 = cputime;

if nargin < 3
    mve_chkdata(A, b);
        
    [msg,x0] = mve_presolve(A, b, maxiter, tol1);
                
    fprintf('  End of Presolve ......\n');
    if msg(1) ~= 's'
        disp(msg);
        return;
    end
else
    mve_chkdata(A, b, x0);
end
[f,E] = mve_solver(A, b, x0, maxiter, tol2);

% E = chol(E2);
% E = E';

fprintf('  CPU time: %g seconds\n', cputime-t0);
