function originalPolytopeCenter=findMVECenter(model)


lb=model.lb;
ub=model.ub;
b = model.b;
S=full(model.S);
[~, ~, G, h] = createPolytopeConstraintsFromBounds(lb, ub);
polytope = Polytope(S, b, G, h);
fprintf('Original dimensionality: %i\n', size(polytope.G, 2));

%% Transform the polytope to a minimal subspace.
polytope.transformToMinimalParametrization('minDimensionThickness',-1);
fprintf('Dimensionality after transformation: %i\n', size(polytope.G, 2));

%% Find the center of the flux space by rounding.
% After optimizing the isotropy (rounding), the polytope is centered at the
% origin. The central point of the original polytope is found by transforming
% the origin back to the original space.
polytope.optimizeIsotropy();
roundedPolytopeCenter = zeros(size(polytope.G, 2), 1);
originalPolytopeCenter = polytope.toOriginal.transform(roundedPolytopeCenter);
fprintf('Polytope center:\n');
disp(originalPolytopeCenter);
end