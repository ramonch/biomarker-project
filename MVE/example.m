% Example usage of the Polytope class for finding the center of a polytope.

%% Create example reaction network.
S = [
    1 -1  0 -1  0;
    0  1 -1  0  0;
    0  0  1  1 -1
];

lb = [0; -10 * ones(4, 1)];
ub =  10 * ones(5, 1);

%% Create flux space polytope.
b = zeros(3, 1);
[~, ~, G, h] = createPolytopeConstraintsFromBounds(lb, ub);
polytope = Polytope(S, b, G, h);
fprintf('Original dimensionality: %i\n', size(polytope.G, 2));

%% Transform the polytope to a minimal subspace.
polytope.transformToMinimalParametrization();
fprintf('Dimensionality after transformation: %i\n', size(polytope.G, 2));

%% Find the center of the flux space by rounding.
% After optimizing the isotropy (rounding), the polytope is centered at the
% origin. The central point of the original polytope is found by transforming
% the origin back to the original space.
polytope.optimizeIsotropy();
roundedPolytopeCenter = zeros(size(polytope.G, 2), 1);
originalPolytopeCenter = polytope.toOriginal.transform(roundedPolytopeCenter);
fprintf('Polytope center:\n');
disp(originalPolytopeCenter);