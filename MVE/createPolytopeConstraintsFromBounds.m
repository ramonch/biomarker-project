function [A, b, G, h] = createPolytopeConstraintsFromBounds(lb, ub, varargin)
% CREATEPOLYTOPECONSTRAINTSFROMBOUNDS Create a set of equality and inequality
% constraints for a polytope given bounds on the coordinates. The constraints
% describe a polytope {x in R^d | A*x = b, G*x <= h}. Dimensions where the lower
% and upper bounds match are constrained by an equality.
%
%       [A, b, G, h] = createPolytopeConstraintsFromBounds([-2; 1], [3; 2]);
%
%   Required:
%       lb:         d-by-1 vector representing the lower bounds of the polytope
%                   variables.
%       ub:         d-by-1 vector representing the upper bounds of the polytope
%                   variables.
%
%   Named parameters:
%       'maxEqualityThreshold': Maximum difference between lower and upper bound
%                   so that an equality constraint is created.
%
%   Returns:
%       A, b, G, h: constraints of polytope in the form:
%                   {x in R^d | A*x = b, G*x <= h}

%  Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
%  Computational Systems Biology group, ETH Z�rich
%
%  This software is freely available under the GNU General Public License v3.
%  See the LICENSE file or http://www.gnu.org/licenses/ for further information.

    %% Parse inputs.
    assert(all(size(lb) == size(ub)) && size(lb, 2) == 1, ...
           'The input bounds have inconsistent dimensions');
    nVariables = size(lb, 1);
       
    parser = inputParser;
    addParameter(parser, 'maxEqualityThreshold', 1e-8);
    parse(parser, varargin{:});
    params = parser.Results;
       
    %% Construct constraints 
    G = [eye(nVariables); -eye(nVariables)];
    h = [ub; -lb];
        
    % Transform pairs pairs of inequalities leading to very thin in equality 
    % constraints. Remove the corresponding inequalities. 
    isEquality = abs(ub - lb) <= params.maxEqualityThreshold;  
    A = G(isEquality, :);
    b = (lb(isEquality) + ub(isEquality)) / 2;
    G = G([~isEquality, ~isEquality], :);
    h = h([~isEquality, ~isEquality]);
end