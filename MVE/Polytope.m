classdef Polytope < handle
% POLYTOPE Class describing a d-dimensional polytope
% {x in R^d | A*x = b, G*x <= h}.

%  Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
%  Computational Systems Biology group, ETH Z�rich
%
%  This software is freely available under the GNU General Public License v3.
%  See the LICENSE file or http://www.gnu.org/licenses/ for further information.

properties(SetAccess = private) 
    % A Left-hand side of the system of equalities in the polytope description.
    A;
    
    % B Right-hand side of the system of equalities in the polytope description.
    b;
    
    % G Left-hand side of the system of inequalities in the polytope
    % description.
    G;
    
    % H Right-hand side of the system of inequalities in the polytope
    % description.
    h;
    
    % TOORIGINAL The affine transformation that must be applied to the
    % polytope to be converted to its original parametrization.
    toOriginal;
end

methods
    function self = Polytope(A, b, G, h)
    % POLYTOPE Construct a new instance of the Polytope class.
    %
    %   Required:
    %       A:          Left-hand side of the system of equalities in the 
    %                   polytope description.
    %       b:          Right-hand side of the system of equalities in the
    %                   polytope description.
    %       G:          Left-hand side of the system of inequalities in the
    %                   polytope description.
    %       h:          Right-hand side of the system of inequalities in the
    %                   polytope description.
    
        if sum(size(A)) == 0
            A = zeros(0, size(G, 2));
            b = zeros(1, 0);
        end
        
        self.A = A;
        self.b = b;
        self.G = G;
        self.h = h;
        self.toOriginal = AffineTransform();
     
        assert(~self.isEmpty(), 'The space defined by the polytope is empty.');
    end
    
    function [segmentsStart, segmentsEnd, rayIndices] = ...
        intersect(self, origins, directions)
    % INTERSECT Compute the intersections of the given rays (defined as
    % ray(i) = origins(:,i) + t*directions(:,i), with t in R) with the space.
    % Each intersection is a segment defined by its starting and ending point.
    %
    %   Requires:
    %       origins:        Origin points of the rays.
    %       directions:     Vectors describing the direction of the rays.
    %
    %   Returns:
    %       segmentsStart:  Value of t at the start of each segment.
    %       segmentsStart:  Value of t at the end of each segment.
    %       rayIndices:     Index of the ray to which each segment belongs. If
    %                       empty, segment i belongs to ray i.
    
        % Verify that the polytope has minimal parametrization and parse
        % inputs.
        if ~isempty(self.A) || ~isempty(self.b)
            error(['Intersection is only possible with polytopes ', ...
                   'represented as interesection of half-spaces only.']);
        end
        nRays = size(directions, 2);
        limits = [-Inf(1, nRays); Inf(1, nRays)];
        
        % Determine orientations of the intersections.
        constraintsDotDirections = self.G * directions;

        % Determine intersection points with all constraints.
        intersectionsT = bsxfun(@minus, self.h, self.G * origins) ...
                         ./ constraintsDotDirections;
        intersectionsT = [intersectionsT; limits];

        % Select the closest intersections in the negative and positive
        % directions.
        intersectionsTpositive = intersectionsT;
        intersectionsTnegative = intersectionsT;
        intersectionsTpositive(intersectionsTpositive <= 0) = Inf;
        intersectionsTnegative(intersectionsTnegative >= 0) = -Inf;
        segmentsStart = max(intersectionsTnegative);
        segmentsEnd = min(intersectionsTpositive);
        rayIndices = [];
    end
    
    function samples = getInitialSamples(self, nSamples, ~)
    % GETINITIALSAMPLES Returns nSamples points that are guaranteed to lie
    % inside the described space. Points are drawn from an approximation of the
    % uniform distribution over the polytope.
    %
    %   Requires:
    %       nSamples:   Number of samples to generate.
    %       pdfSampler: Sampler describing the pdf over the space.
    % 
    %   Returns:
    %       samples:    d-by-nSamples vector containing the initial samples.
    
        if ~isempty(self.A) || ~isempty(self.b)
            error(['For optimal results, the polytope should be ', ...
                   'represented as interesection of half-spaces only.']);
        end
        
        nDimensions = size(self.G, 2);
    
        % Find the center of the maximum volume ball inscribed in the polytope.
        [center, ~] = mve_run(self.G, self.h);

        % Compute distances from the center to the constraints.
        normG = vecnorm(self.G, 2, 2);
        distances = abs((self.h - self.G * center) ./ normG);
        if(max(distances) - min(distances) > 0.0001)
            fprintf(['Some faces of the polytope do not touch the maximum ' ...
                     'volume inscribed ball.\n']);
        end

        % Generate uniform samples from the maximum unit ball.
        directions = randn(nDimensions, nSamples);
        sampleDistancesFromCenter = ...
            rand(1, nSamples).^(1 / nDimensions) ./ vecnorm(directions); 
        directions = bsxfun(@times, directions, sampleDistancesFromCenter);

        % Transform samples from the maximum volume ball to the polytope.
        % Map the samples from the chord between center and surface to the chord
        % between center and the closest constraint.
        % TODO: replace 0.99 with a parameter.
        [~, tPositive] = self.intersect(repmat(center, 1, nSamples), directions);
        samples = bsxfun(@plus, bsxfun(@times, directions, tPositive * 0.99), center);
    end
    
    function isEmpty = isEmpty(self)
    % ISEMPTY Verifies whether the polytope is empty.
    %
    %   Returns:
    %       isEmpty:    True if the polytope is empty, false otherwise.
    
        [~, nDimensions] = size(self.G);
        [~, ~, exitFlag] = solveLinearProgram( ...
            zeros(1, nDimensions), self.G, self.h, self.A, self.b);
        isEmpty = (exitFlag ~= 1);
    end
    
    function applyTransform(self, transform, shift)
    % APPLYTRANSFORM Apply an affine transform to the polytope.
    % 
    %   Required:
    %       transform:  Linear transformation to apply to the polytope.
    %       shift:      Affine shift to apply to the polytope.
    
        self.toOriginal = self.toOriginal.applyTransform( ...
            AffineTransform(transform, shift));
        self.h = self.h - self.G * shift;
        self.G = self.G * transform;
        self.b = self.b - self.A * shift;
        self.A = self.A * transform;
    end

    function [center, radius] = getChebyshevCenter(self)
    % GETCHEBYSHEVCENTER Get the Chebyshev center of the polytope, i.e. the
    % center of the largest ball inscribed in the polytope.
    %
    %   Returns:
    %       center:     Chebyshev center of the polytope.
    %       radius:     Distance from the Chebyshev center to the closest point
    %                   on the boundary of the polytope.
    
        nDimensions = size(self.G, 2);
        rowNorms = vecnorm(self.G, 2, 2);

        objective = [zeros(nDimensions, 1); 1];
        % TODO: prevent that narrow polytopes cause errors later in the
        % rounding.
        [result, radius, exitFlag] = solveLinearProgram( ...
            objective, [self.G rowNorms], self.h);
        
        if exitFlag ~= 1
            % The most likely reasons are empty polytope and one (or more)
            % dimension being too thin/large, causing numerical problems.
            error('Error searching the Chebyshev center of the polytope.');
        end
        center = result(1:nDimensions);    
    end
    
    function toOriginal = transformToMinimalParametrization( ...
        self, varargin)
    % TRANSFORMTOMINIMALPARAMETRIZATION Transform the polytope to an minimal
    % parametrization where:
    %   1) [Optional] No thin dimension is present.
    %   2) No equality constraint is present (half-space representation).
    %   3) No redundant inequality is present.
    %
    %   Named parameters:
    %       'minDimensionThickness': Minimum thickness of the polytope
    %               perpendicular to a constraint. Thinner dimensions are
    %               squeezed in an equality constraint. If negative, no
    %               flattening is performed.
    %       'minDistanceForRedundancy': Minimum distance between an
    %               inequality constraint and the closest point of the polytope
    %               so that the constraint can be considered redundant.
    %       'maxNormForZeroConstraint': Maximum norm of an inequality
    %               constraint so that it is considered orthogonal to the
    %               polytope.
    %
    %   Returns:
    %       affineTransform:        Affine transform from the optimal to the
    %                               original parametrization.
    
        % Read options.
        parser = inputParser;
        addParameter(parser, 'minDimensionThickness', 0);
        addParameter(parser, 'minDistanceForRedundancy', 1e-4);
        addParameter(parser, 'maxNormForZeroConstraint', 1e-6);
        addParameter(parser, 'maxNonZeroFractionForSparseG', 1 / 20);
        parse(parser, varargin{:});
        options = parser.Results;
        
        fprintf('Tranforming polytope to simplified parametrization\n');
        
        if options.minDimensionThickness >= 0
            fprintf('  Flattening thin dimensions ...\n');
            self.flattenThinDimensions(options.minDimensionThickness, ...
                                       options.maxNormForZeroConstraint);
        end
        
        fprintf('  Tansforming to minimal subspace ...\n');
        self.transformToMinimalSubspace();
        
        fprintf('  Removing redundant constraints ...\n');
        self.removeRedundantConstraints(options.minDistanceForRedundancy, ...
                                        options.maxNormForZeroConstraint);
                            
        toOriginal = self.toOriginal;
        if nnz(self.G) <= numel(self.G) * options.maxNonZeroFractionForSparseG
            fprintf('  Using sparse representation of the constaints ...\n');
            self.G = sparse(self.G);
        end
        fprintf('Done\n');
    end
    
    function [descriptor, fromOptimalFrame, toOptimalFrame] = ...
        getOptimallyReparametrizedDescriptor(self, optimalityCriterion)
    % GETOPTIMALLYREPARAMETRIZEDDESCRIPTOR Create a re-parametrized version of 
    % this descriptor that is optimal according to the specified criterion.
    %
    %   Requires:
    %       optimalityCriterion: String containing the name of the optimality
    %       criterion.
    %
    %   Returns:
    %       descriptor:         Reparametrized version of this descriptor.
    %       fromOptimalFrame:   Affine transformation from the coordinate frame
    %                           of the returned descriptor to the original 
    %                           frame.
    %       toOptimalFrame:     Affine transformation from the original frame to
    %                           the coordinate frame of the returned descriptor.
    
        if strcmp(optimalityCriterion, 'isotropy')
            assert(isempty(self.A) && isempty(self.b), ['Isotropy ' ...
                'optimization is available only for polytopes ', ...
                'represented as interesection of half-spaces only.']);
            
            % Create a polytope with improved isotropy.
            descriptor = Polytope(self.A, self.b, self.G, self.h);
            descriptor.optimizeIsotropy();
            fromOptimalFrame = descriptor.toOriginal;
            toOptimalFrame = fromOptimalFrame.getInverse();
        elseif strcmp(optimalityCriterion, 'intersection')
            % We assume that the original parametrization is already the most
            % sparse possible (i.e. the fastest to compute intersections on).
            % If not consider improving this method.
            descriptor = Polytope(self.A, self.b, self.G, self.h);
            fromOptimalFrame = AffineTransform();
            toOptimalFrame = AffineTransform();
        else
            warning(['The ''%s'' optimality criterion is not recognized ' ...
                'by the Polytope descriptor, consider implementing it. ' ...
                'Returning the original descriptor.'], optimalityCriterion);
            descriptor = Polytope(self.A, self.b, self.G, self.h);
            fromOptimalFrame = AffineTransform();
            toOptimalFrame = AffineTransform();
        end
    end
    
    function optimizeIsotropy(self)
    % OPTIMIZEISOTROPY Find a mapping of the polytope to a parametrization of
    % omptimal isotropy. This is done by finding the maximum volume inscribed
    % ellipsoid and mapping it to a ball.
    %
    %   Based on preprocess() by Ben Cousins:
    %   https://github.com/Bounciness/Volume-and-Sampling
    
        % Precondition the inequalities matrix to reduce numerical precision
        % problems.
        nDimensions = size(self.G, 2);
        [columnScale, rowScale] = gmscale(self.G, 0, 0.99);
        self.G = diag(1./rowScale) * self.G * diag(1./columnScale);
        self.h = diag(1./rowScale) * self.h;
        self.toOriginal = self.toOriginal.applyTransform( ...
            AffineTransform(diag(1./columnScale), zeros(nDimensions, 1)));
        
        % Normalize each row in order to have similar scaling.
        rowNorms = vecnorm(self.G, 2, 2);
        self.G = diag(1./rowNorms) * self.G;
        self.h = diag(1./rowNorms) * self.h;

        % Perform rounding iteratively. Should make the problem easier to solve
        % numerically.
        maxIterations = 20;
        iteration = 0;
        reg = 1e-3;
        iterationTransform = eye(nDimensions);
        converged = 0;
        while (max(eig(iterationTransform)) > 6*min(eig(iterationTransform)) ...
                && converged ~= 1) || reg > 1e-6 || converged == 2
            tic;
            iteration = iteration + 1;
            [center, distance] = self.getChebyshevCenter();
            reg = max(reg / 10, 1e-10);
            
            % Estimate the maximum volume enclosed ellipoid and transform the
            % polytope mapping it to a ball.
            [iterationShift, iterationTransform, converged] = mve_run_cobra( ...
                self.G, self.h, center, reg);
            self.applyTransform(iterationTransform, iterationShift);
            
            % Normalize each row in order to restore similar scaling.
            rowNorms = vecnorm(self.G, 2, 2);
            self.G = diag(1./rowNorms) * self.G;
            self.h = diag(1./rowNorms) * self.h;
            if iteration == maxIterations
                break;
            end

            fprintf(['Iteration %d: reg=%.1e, ellipsoid vol=%.1e, longest ' ...
                     'axis=%.1e, shortest axis=%.1e, x0 dist to bdry=%.1e, ' ...
                     'time=%.1e seconds.\n'], iteration, reg, ...
                     det(iterationTransform), max(eig(iterationTransform)), ...
                     min(eig(iterationTransform)), distance, toc);
        end

        if iteration == maxIterations
            fprintf(['Maximum number of iterations reached, rounding may ' ...
                     'not be ideal.\n']);
        end
    
        fprintf('Maximum volume ellipsoid found.\n');
        if min(self.h) <= 0
            [center, ~] = self.getChebyshevCenter();
            self.applyTransform(eye(dim), center);
            fprintf(['Shifting so that the origin is inside the polytope. ' ...
                     'rounding may not be ideal.\n']);
        end
    end
end

methods (Access = protected)
    function flattenThinDimensions(self, thresholdThickness, ...
                                   maxNormForZeroConstraint)
    % FLATTENTHINDIMENSIONS Identifies directions in which the space is thin
    % enough to be considered flat and enforce the flatness with equality
    % constraints.

        nInequalities = size(self.G, 1);
        isToIgnore = false(nInequalities, 1);
        isToConvertToEquality = false(nInequalities, 1);
        newEqualitiesShifts = nan(nInequalities, 1);
        previousMessageLength = 0;
        
        % Find the width of the space generated by each constraint. This is done
        % by maximizing and minimizing an objective function perpendicular to
        % the normal of each constraint.
        for iInequality = nInequalities:-1:1
            newMessage = sprintf('    Processing inequality %d of %d ...\n', ...
                                 nInequalities - iInequality, nInequalities);
            fprintf([repmat('\b', 1, previousMessageLength), newMessage]);
            previousMessageLength = length(newMessage);
            
            % Temporarily reduce the polytope to a minimal subspace.
            [G_opt, h_opt] = self.getMinimalSubspace( ...
                [self.A; self.G(isToConvertToEquality, :)], ...
                [self.b; newEqualitiesShifts(isToConvertToEquality, :)], ...
                self.G(~isToIgnore, :), self.h(~isToIgnore, :));
            
            % Ignore inequalities that are orthogonal to the polytope. Actual
            % removal is performed in a later step.
            if norm(G_opt(iInequality, :)) < maxNormForZeroConstraint
                isToIgnore(iInequality) = true;
                continue;
            end
            
            % Find the width of the space in the direciton perpendicular to the
            % current constraint.
            objective = G_opt(iInequality, :) / norm(G_opt(iInequality, :));
            [x, tNegative] = solveLinearProgram(-objective, G_opt, h_opt);
            [~, tPositive] = solveLinearProgram(objective, G_opt, h_opt);
            tNegative = tNegative + h_opt(iInequality);
            tPositive = tPositive - h_opt(iInequality);
            
            % If t is negative in one of the two directions, then the constraint
            % is redundant (unless there are numerical errors). If the width of
            % the space in the given direction is below the given threshold we
            % consider the dimension as flat and the current inequality is
            % inserted as equality. Removal of redundand inequalities is
            % performed in a later step.
            if tPositive < 0 || tNegative < 0
                isToIgnore(iInequality) = true;
            elseif tPositive + tNegative < thresholdThickness
                isToIgnore(iInequality) = true;
                isToConvertToEquality(iInequality) = true;
                newEqualitiesShifts(iInequality) = G_opt(iInequality, :) * x;
            end
        end
        
        % Update the polytope according to the new constraints.
        self.A = [self.A ; self.G(isToConvertToEquality, :)];
        self.b = [self.b ; self.h(isToConvertToEquality, :)];
        self.G = self.G(~isToConvertToEquality, :);
        self.h = self.h(~isToConvertToEquality); 
        
        fprintf(['  Identified %d thin inequalities, %d of which have been ' ...
                 'used as equalities.\n'], sum(isToIgnore), ...
                 sum(isToConvertToEquality));
    end  
    
    function [G, h] = getMinimalSubspace(~, A, b, G, h)    
        if ~isempty(A)
            N = null(A, 'r');
            
            % We tolerate the warning since the space has not been optimized
            % yet.
            warning('off', 'MATLAB:rankDeficientMatrix');
            internalPoint = A \ b;
            warning('on', 'MATLAB:rankDeficientMatrix');
            
            h = h - G * internalPoint;
            G = G * N;
        end
    end    
    
    function removeRedundantConstraints(self, minDistanceForRedundancy, ...
                                        maxNormForZeroConstraint)
    % REMOVEREDUNTANTCONSTRAINTS Remove from the definition of the polytope all
    % redundant inequality constraints.
        
        % First, remove duplicate constraints.
        [~, uniqueConstraintsIndices, ~] = unique([self.G, self.h], 'rows');
        self.G = self.G(uniqueConstraintsIndices, :);
        self.h = self.h(uniqueConstraintsIndices, :);
        
        
        
        % First, remove constraints that are obviously redundant
        [~, ia, ic] = unique(self.G, 'rows');
        G_tmp=zeros(length(ia),size(self.G,2));
        h_tmp=zeros(length(ia),1);
        for zc=1:length(ia)
            identicalConstraints=find(ic==zc);
            G_tmp(zc,:)=self.G(identicalConstraints(1),:);
            minConstraint=min(self.h(identicalConstraints));
            h_tmp(zc)=minConstraint;
        end
        self.G=G_tmp;
        self.h=h_tmp;
        
        nConstraints = size(self.G, 1);
        isRedundant = false(nConstraints, 1);
        
        % Local copies in order to avoid broadcast of self in the parfor.
        G_tmp = self.G;
        h_tmp = self.h;
        nConstraints  
        dist=zeros(nConstraints, 1);
        runConstraints=false;
        if runConstraints
        parfor iConstraint = 1:nConstraints  
            iConstraint
            vConstraint=false(nConstraints,1);
            vConstraint(iConstraint)=true;
            if norm(G_tmp(iConstraint, :)) < maxNormForZeroConstraint
                % Remove inequalities that are orthogonal to the polytope.
                isRedundant(iConstraint) = true;
                continue;
            else            
                % Remove constraints that don't have at least one contact point
                % with the polytope.
                % TODO: Verify if this can be sped up using the following rule:
                % a constraint is redundant if the polytope generated by
                % flipping its direciton is empty.
                
                selCons=~vConstraint;
                
                LHS=[G_tmp(selCons,:);G_tmp(vConstraint,:)];
                RHS=[h_tmp(selCons,:);h_tmp(vConstraint,:)];
                [~, minDistance] = ...
                    solveLinearProgram(LHS(end,:), LHS, RHS);
                distanceToPolytope =h_tmp(iConstraint)-minDistance;
                isRedundant(iConstraint) = ...
                    distanceToPolytope >= minDistanceForRedundancy;
                dist(iConstraint)=distanceToPolytope;
            end
        end
        end
        self.G = self.G(~isRedundant, :);
        self.h = self.h(~isRedundant);
        fprintf(['  Removed %d redundant constraints, %d left in the ' ...
                 'description.\n'], sum(isRedundant), size(self.G, 1));
    end
    
    function transformToMinimalSubspace(self)
    % TRANSFORMTOMINIMALSUBSPACE Transform the polytope to a minimal subspace
    % where its description is only composed by inequality constraints.
    
        if ~isempty(self.A)
            nOriginalDimensions = size(self.G, 2);
            N = getNullSpace(sparse(self.A));
            internalPoint = self.A \ self.b;
            self.toOriginal = self.toOriginal.applyTransform( ...
                AffineTransform(full(N), internalPoint));
            
            self.h = self.h - self.G * internalPoint;
            self.G = self.G * full(N);
            self.b = zeros(0, 1);
            self.A = zeros(0, size(self.G, 2));
            fprintf(['  %d-dimensional polytope description reduced to a ' ...
                     '%d-dimensional subspace with %d constraints.\n'], ...
                     nOriginalDimensions, size(self.G, 2), size(self.G, 1));
        end
    end   
end

end