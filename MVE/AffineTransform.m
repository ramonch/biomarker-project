classdef AffineTransform
% AFFINETRANSFORM represents an affine transform:
%   x' -> x : x = T*x' + translation

%  Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
%  Computational Systems Biology group, ETH Z�rich
%
%  This software is freely available under the GNU General Public License v3.
%  See the LICENSE file or http://www.gnu.org/licenses/ for further information.

properties
    % T Linear transfrom matrix.
    T;
    
    % SHIFT Translation vector.
    shift;
    
    % ISIDENTITY If true, this transformation represents the identity transform.
    isIdentity;
end
    
methods
    
    function self = AffineTransform(T, shift)
    % AFFINETRANSOFRM Constructs a new instance of the AffineTransform class. If
    % no argument i sspecified, an identity transform will be created.
    %
    %   Optional:
    %       T:          Linear transform matrix.
    %       shift:      Translation vector.
        
        self.isIdentity = (nargin == 0);
        if ~self.isIdentity
            self.T = T;
            self.shift = shift;
        end
    end
        
    function transformedVectors = transform(self, vectors)
    % TRANSFORM Apply the affine transform to a list of vectors.
    %  
    %   Requires:
    %       vectors:    Matrix containing the vectors to be transformed as
    %                   columns.
    %
    %   Returns:
    %       transformedVectors: Matrix containin the transformed vectors as
    %                   columns.
        
        if self.isIdentity
            transformedVectors = vectors;
        else
            transformedVectors = bsxfun(@plus, self.T * vectors, self.shift);
        end
    end
    
    function transformedVectors = transformLinear(self, vectors)
    % TRANSFORM Apply the linear part of the transform to a list of vectors.
    %  
    %   Requires:
    %       vectors:    Matrix containing the vectors to be transformed as
    %                   columns.
    %
    %   Returns:
    %       transformedVectors: Matrix containin the transformed vectors as
    %                   columns.
        
        if self.isIdentity
            transformedVectors = vectors;
        else
            transformedVectors = self.T * vectors;
        end
    end
        
    function transformedVectors = inverseTransform(self, vectors)
    % INVERSETRANSFORM Apply the inverse of the affine transform to a list of
    % vectors.
    %  
    %   Requires:
    %       vectors:    Matrix containing the vectors to be transformed as
    %                   columns.
    %
    %   Returns:
    %       transformedVectors: Matrix containin the transformed vectors as
    %                   columns.
        if self.isIdentity
            transformedVectors = vectors;
        else
            transformedVectors = self.T \ bsxfun(@minus, vectors, self.shift);
        end
    end
    
    function self = applyTransform(self, transform)
    % APPLYTRANSFORM Combines this transform with the given affine transform.
    %
    %   Requires:
    %       transform:  Linear transform to be combined with this transform.
        
        if ~transform.isIdentity
            if self.isIdentity
                self.shift = transform.shift;
                self.T = transform.T;
                self.isIdentity = false;
            else
                self.shift = self.T * transform.shift + self.shift;
                self.T = self.T * transform.T;
            end
        end
    end
    
    function inverseTransform = getInverse(self)
    % GETINVERSE Returns the inverse of this transform.
    %
    %   Returns:
    %       inverseTransform: The inverse affine transform.
    
        if self.isIdentity
            inverseTransform = AffineTransform();
        else
            invT = inv(self.T);
            inverseTransform = AffineTransform(invT, -invT * self.shift);
        end
    end
    
    function linearTransform = getLinearTransform(self)
    % GETLINEARTRANSFORM Returns the linear part of this transform.
    %
    %   Returns:
    %       linearTransform: LinearTransform containing the linear part of this
    %                        transform.
    
       linearTransform = LinearTransform(self.T); 
    end
end
    
end

