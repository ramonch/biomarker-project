function model=createToyModelBiomarkers(modeltype)


switch modeltype
    case 1 
N=[...
    -1   0   0   0   -1  -1  0   0   0;...
    0   -1   0   0   1  0    -1  0   0;...
    0   0   0   0   0   1   0   -1  -1;...
    0   0   -1  0   0   0   1   1   0;...
    0   0   0   -1  0   0   0   0   1];

nr=size(N,2);
nm=size(N,1);
model.S=N;
model.lb=zeros(nr,1);
model.lb(1:2)=-1;
model.ub=10*ones(nr,1);
model.rxnNames={'EX_A','EX_B','EX_D','EX_E','R1','R2','R3','R4','R5'}';
model.rxns=model.rxnNames;
ng=5;

model.rxnGeneMat=[zeros(4,ng);eye(ng)];
model.genes=strcat('g_',sprintfc('%g',1:ng))';

model.rules=[repmat({''},4,1);strcat('x(',sprintfc('%g',1:ng),')')'];
model.c=zeros(nr,1);
model.b=zeros(nm,1);
    case 2
        N=[
            1   0   -1  -1  0   0   0   0
            0   1   0   0   -1  0   0   0
            0   0   0   1   1   -1  0   0
            0   0   1   0   0   1   -1  0
            0   0   1   0   0   0   0   -1];
   

    nr=size(N,2);
    nm=size(N,1);
    model.S=N;
    model.lb=zeros(nr,1);
    model.lb(1:2)=-1;
    model.ub=10*ones(nr,1);
    model.rxnNames=strcat('R_',sprintfc('%g',1:nr))';
    model.rxns=model.rxnNames;
    ng=4;

    model.rxnGeneMat=[zeros(2,ng);eye(ng);zeros(2,ng);];
    model.genes=strcat('g_',sprintfc('%g',1:ng))';

    model.rules=[repmat({''},2,1);strcat('x(',sprintfc('%g',1:ng),')')';repmat({''},2,1)];
    model.c=zeros(nr,1);
    model.b=zeros(nm,1);
    model.mets=strcat('m_',sprintfc('%g',1:nm))';

end
    
    