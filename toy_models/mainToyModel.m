
clear 
close all


%%
maxFVA=0;
flFVA=0;
flpert=1;
modeltype=2;
%%
model=createToyModelBiomarkers(modeltype);


%% Reference flux
vref=findMVECenter(model);
%[2,2,1,1,2,3,4,1]';


%%

ng=length(model.genes);

g=table;
g.Genes=model.genes;
g.Id=model.genes;

% MSUD is supposed to inhibit via the ketoacids OGDH 
g.idm=findGeneIDs(model,g.Id);

% 
for z=1:ng
    [zrxn,zconstrainrxn,~]=detectAffectedRxnsByGene(model,g.Id(z));
    g.affectedRxn{z}=zrxn(zconstrainrxn);
end
%%
biomark.abbrev=model.rxns([1:8]);

biomidx=findRxnIDs(model,biomark.abbrev);
%% Sensitivity Matrix
tolerance =10^(-12);
SM=create_feature_matrix_rxns_biomarker(model, 1:length(model.rxns),tolerance);

%%
nbiom=length(biomark.abbrev);
flDisplayFigure=true;
percentages=0.001;
[listbiomark,madbiomark]=deal(nan(ng,nbiom));
listbiomarkRup=nan(ng,nbiom);
ig=4;
exbool=startsWith(model.rxns, 'EX_');
% [listbiomark, madbiomark]=predict_biomarkers_sens_genes(model,g(ig,:), SM,...
%     biomidx,biomark.abbrev,flDisplayFigure,tolerance,1);

[listbiomark2,madBiomark2,res2]=predict_biomarkers_sens(model,model.S,g.affectedRxn{ig}, SM,biomidx,biomark.abbrev,...
    flDisplayFigure,tolerance,1,exbool);

[listbiomarkRup,~]=predict_biomarkers_ruppin_genes(model,g(ig,:),...
    biomark.abbrev,percentages,maxFVA,flFVA,flpert);

%%
figure
HM=HeatMap(flipud(listbiomark2), 'ColumnLabels',biomark.abbrev,...
    'ColorMap','redbluecmap','RowLabels',flipud(g.Id(ig)),'Annotate',true);


figure
HM=HeatMap(flipud(listbiomarkRup), 'ColumnLabels',biomark.abbrev,...
    'ColorMap','redbluecmap','RowLabels',flipud(g.Id(ig)),'Annotate',true);
