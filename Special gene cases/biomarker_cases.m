%% 
clear 
close all hidden

%% Load models 
testcase=10;
filetestcase=['../initialization files/testcase' num2str(testcase) '_init.mat'];
if exist(filetestcase,'file')
    load(filetestcase)
end
%%
folderGene='../Data publications/';
folderModel='../Models/';
flDisplayFigure=true;
tolerance =10^(-12);
fluseAllExch=0;
%%
initializeBiomarkerPrediction;

if ~exist(filetestcase,'file')
    save(['../initialization files/testcase' num2str(testcase) '_init.mat'])
end

%% Reduce model and SM Matrix 
% leftrxn=find(~isnan(SM(:,1)));
% SMred=SM(leftrxn,leftrxn);
% humanred=human;
% humanred.rxns=human.rxns(leftrxn);
% humanred.rules=human.rules(leftrxn);
% humanred.S=human.S(:,leftrxn);
% humanred.lb=human.lb(leftrxn);
% humanred.ub=human.ub(leftrxn);
% humanred.c=human.c(leftrxn);
% 
% 
% if isfield(human,'rxnGeneMat')
%     humanred.rxnGeneMat=human.rxnGeneMat(leftrxn,:);
% end
% 
% [~, biomidxred]=ismember(biomidx,leftrxn);
%%
Names={'PCCA','PCCB','IVD','ARG1','OTC','HAL'}';
Id={'5095.1','5096.1','3712.1','383.1','5009.1','3034.1'}';
% Names={'ARG','DLD','DBT','BCKDHA','BCKDHB','OGDH','MT-ND1'}';
% Id={'383.1','1738.1','1629.1','593.1','594.1','4967.1','4535.1'}';
g=table;
g.Genes=Names;
g.Id=Id;

% MSUD is supposed to inhibit via the ketoacids OGDH 
g.idm=findGeneIDs(humanred,g.Id);

% 
for z=1:size(g,1)
    [zrxn,zconstrainrxn,~]=detectAffectedRxnsByGene(humanred,g.Id(z));
    g.affectedRxn{z}=zrxn(zconstrainrxn);
end

% 
% for z=1:size(g,1)-1
%     zs=size(g,1);
%     g.Genes(zs+1)=strcat(g.Genes(z),'-',g.Genes(5));
%     g.affectedRxn{zs+1}=union(g.affectedRxn{z},g.affectedRxn{5});
% end
%%
ng=size(g,1);
nbiom=length(biomark.abbrev);
percentages=0.001;
[listbiomark,madbiomark]=deal(nan(ng,nbiom));
listbiomarkRup=nan(ng,nbiom);


ig=ng;%1:ng;
for zg=ig
    zg
    [zlistbiomark, zmadbiomark,res]=predict_biomarkers_sens(humanred,full(humanred.S),g.affectedRxn{zg}, SMred,...
        biomidxred,biomark.abbrev,flDisplayFigure,tolerance,2.5);
    

    listbiomark(zg,:)=zlistbiomark;
    madbiomark(zg,:)=zmadbiomark;
end

%%
figure 

iex=find(startsWith(humanred.rxns,'EX_ile'));

histogram(SMred(:,iex))
xlim([-1,1])
%%
figure
HM=HeatMap(flipud(listbiomark(ig,:)), 'ColumnLabels',biomark.biom,...
    'ColorMap','redbluecmap','RowLabels',flipud(g.Genes(ig)),'Annotate',true);

figure
madbiomark2=madbiomark;
madbiomark2(madbiomark2<3)=0;
HM=HeatMap(flipud(madbiomark2(ig,:)), 'ColumnLabels',biomark.biom,...
    'ColorMap','redbluecmap','RowLabels',flipud(g.Genes(ig)),'Annotate',true,'AnnotColor','k');


ig=6%4:5
listbiomark2=listbiomark;
listbiomark2(madbiomark<5)=0;
HM=HeatMap(flipud(listbiomark2(ig,:)), 'ColumnLabels',biomark.biom,...
    'ColorMap','redbluecmap','RowLabels',flipud(g.Genes(ig)),'Annotate',true,'AnnotColor','k');
plot(HM)

figure
HM=HeatMap(flipud(listbiomarkRup(ig,:)), 'ColumnLabels',biomark.biom,...
    'ColorMap','redbluecmap','RowLabels',flipud(g.Genes(ig)),'Annotate',true);

