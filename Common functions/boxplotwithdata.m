function foo = boxplotwithdata(data,toplot,group, varargin) 

%% Standard function parameters

foo=figure
untypes=unique(data.(group));
n = length(untypes);
d = cell(n,1);
for z = 1:n
    if ~iscell(data)
        d{z} = data.(toplot)(strcmp(data.(group),untypes(z)));
    end
end


[xt,hd,hm] = dotplot(d);
defaultCols = get(gca,'ColorOrder');
for z = 1:n
    set(hd{z},'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2, ...
        'MarkerFaceColor',defaultCols(1,:),'MarkerEdgeColor',defaultCols(1,:),...
        'SizeData',12);
    set(hm{z},'Color',defaultCols(3,:),'LineWidth',1);
end

%% Scaling and formatting
hold on;
plot([.5 n+.5],[0 0],'k--');
set(gca,'XTick',1:n,'XTickLabel',untypes,'XTickLabelRotation',45);

