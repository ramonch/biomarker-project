function [exc_reac,exc_reac_ids,exfound]=findExcRxnFromMets(model,list_mets)

% This functions retrieves the id of the exchange reactions
list_mets=cellfun(@(c) strtrim(c),list_mets,'uni',0);
list_mets_i=cellfun(@(a) horzcat(a,'[e]'),list_mets,'uni',0);
mets_ids=findMetIDs(model,list_mets_i);
exc_reac_ids=nan(1,length(mets_ids));
if any(mets_ids)==0
    i_met_ids_null=find(mets_ids==0);
    error('The ids for the following metabolites could not be retrieved %s \n',list_mets(i_met_ids_null));
end
for i=1:length(mets_ids)
    if mets_ids(i)~=0
       column=zeros(length(model.S(:,1)),1);
       column(mets_ids(i))=-1;
       [~,exc_reac_id]=ismember(model.S',column','rows');
       ireac=find(exc_reac_id);
       if ~isempty(ireac)
        exc_reac_ids(i)=ireac;
       end
    end
end
   exfound=~isnan(exc_reac_ids);
   exc_reac=model.rxns(exc_reac_ids(exfound));
end