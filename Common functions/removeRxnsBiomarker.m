function modelred=removeRxnsBiomarker(model,rxnlist)
%rxn list is the list of reactions to remove
rxnlistidx=findRxnIDs(model,rxnlist);
leftrxn=setdiff(1:length(model.rxns),rxnlistidx);

modelred=model;
modelred.rxns=model.rxns(leftrxn);
modelred.rules=model.rules(leftrxn);
modelred.S=model.S(:,leftrxn);
modelred.lb=model.lb(leftrxn);
modelred.ub=model.ub(leftrxn);
modelred.c=model.c(leftrxn);
modelred.rxnNames=model.rxnNames(leftrxn);


if isfield(model,'rxnGeneMat')
    modelred.rxnGeneMat=model.rxnGeneMat(leftrxn,:);
end

if isfield(model,'subSystems')
    modelred.subSystems=model.subSystems(leftrxn);
end
if isfield(model,'rxnConfidenceScores')
    modelred.rxnConfidenceScores=model.rxnConfidenceScores(leftrxn);
end
