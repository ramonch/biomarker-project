function hasIsozyme=evaluateCbRule(model,rxnID)
rule=model.grRules(rxnID);
cell_rules=strsplit(rule{:},'or');
if length(cell_rules)>1
    hasIsozyme=true;
else
    hasIsozyme=false;
end

end