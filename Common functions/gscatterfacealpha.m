function gscatterfacealpha(x,y,gp,color,alpha)

figure
un=unique(gp);
for i=1:length(un)
    idx=strcmp(gp,un{i});
    s=scatter(x(idx),y(idx),[],color(i,:),'filled');
    s.MarkerFaceAlpha=alpha;
    hold on
end
legend(un)