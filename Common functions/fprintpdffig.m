%% Function: Print pdf figure w/ adaptation of paper size

function [varargout] = fprintpdffig(fignr, fn,exportType)
et=['-d' exportType];



f       = figure(fignr);
fu0     = f.Units;
f.Units = f.PaperUnits;
ha = [findobj(f,'Type','Axes'); findobj(f,'Type','Legend')];
na = length(ha);

% outer position left bottom width height
opos = zeros(na,4);
for z = 1:length(ha)
    if strcmp(ha(z).Type,'axes')
        opos(z,:) = ha(z).OuterPosition;
    else
        opos(z,:) = ha(z).Position;
    end
end
% expos min(left) min(bottom) max(right) max(top)
%exdim max width max(height)
expos = [opos(:,1) opos(:,2) opos(:,1)+opos(:,3) opos(:,2)+opos(:,4)];
expos = [min(expos(:,1)) min(expos(:,2)) max(expos(:,3)) max(expos(:,4))];
exdim = [expos(3)-expos(1) expos(4)-expos(2)];

fpos = f.OuterPosition;
fdim = [fpos(3) fpos(4)];

f.PaperSize = exdim.*fdim;
f.Units = fu0;

warning off;
print(f,et,fn,'-painters');
warning on;

varargout{1} = f;

return
