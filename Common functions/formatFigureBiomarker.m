function formatFigureBiomarker(f1,ratio)
set(findobj(gca,'type','line'),'linew',0.5)
set(gca,'Fontsize',8,'FontName','Arial')
set(gca,'box','on')

f1.Position(3:4)=ratio*f1.Position(3:4);

f1.PaperPositionMode = 'auto';
fig_pos = f1.PaperPosition;
f1.PaperSize = [fig_pos(3) fig_pos(4)];
%f1.PaperPosition([1,2])=[0,1];