function varargout = dotplot(data, varargin) 

%% Standard function parameters

coldata = [0 0 0]; %.5+[0 0 0]; % data point color
colmark = [1 0 0]; % color markers for mean etc.
dxmax   = 0.4;
dx0     = 0.05;

PLevel = [inf .05 .01 .001];
PSymbol = {'n.s.','*','**','***'};

% convert to matrix if necessary
if iscell(data)
    nd = cellfun(@numel,data);
    lx = length(data);
    datam = nan(max(nd),lx);
    for z = 1:lx
        datam(1:nd(z),z) = data{z}(:);
    end
    data = datam;
end
lx = size(data,2);
ly = size(data,1);

FlPaired = false; % paired samples
FlPLines = false; % indicate pairing by lines
FlDLabel = false; % label rel. difference for paired samples

[ht,hll] = deal([]);

%% Optional Arguments

if nargin==1
    xt = 1:lx; % X-Ticks
else
    xt = varargin{1};
end

for z = 2:(nargin-1)
    za = varargin{z};
    
    switch za
        case 'Paired'
            FlPaired = true;
        case 'PairedLines'
            FlPaired = true;
            FlPLines = true;
        case 'LabelDiff'
            FlDLabel = true;
    end
end

%% Statistics

nbin    = max(10,min(ceil(ly/10),50));

dmean   = nanmean(data); % Mean
dci     = nanstd(data); %*tinv(0.975,ly-1); % 95% Confidence Intervals

xtd     = repmat(xt, ly, 1); % X-Ticks For Data
sb      = [xt'-ones(lx,1)*dxmax/2,  xt'+ones(lx,1)*dxmax/2]; % Short Bar X
lb      = [xt'-ones(lx,1)*dxmax,  xt'+ones(lx,1)*dxmax]; % Long Bar X

%% Calculate point positions and plot

[hd,hm] = deal(cell(1,lx));
dmaxs   = zeros(1,lx);

for zc = 1:lx
    
    d = data(:,zc);
    d = d(~isnan(d));
    
    if ~isempty(d)
    dmin = min(d);
    dmax = max(d);
    dmaxs(zc) = dmax;
    
    xbin = linspace(dmin,dmax, nbin+1);
    [n, bin] = histc(d,xbin);
    dx = min(2*dxmax/max(n),dx0); % horiz. distance between points
    
    for z = 1:length(n)
        if n(z)
            idx = find(bin==z);
            ni  = length(idx);
            %dx = min(2*dxmax/ni,dx0); % horiz. distance between points
            if mod(ni,2) % uneven # points
                xinc = dx*(-floor(ni/2):+floor(ni/2));
            else
                xinc = dx/2:dx:((ni/2-1)*dx+dx/2);
                xinc = [-xinc +xinc];
            end
            xtd(idx,zc) = xtd(idx,zc)+xinc';
        end
    end
    end
%     hd{zc} = plot(xtd(:,zc), data(:,zc), 'o');
%     set(hd{zc}, 'MarkerFaceColor', coldata, 'MarkerEdgeColor', coldata);
%     hold on
%     
%     hm{zc} = [plot(lb(zc,:), [1 1]*dmean(zc)); ...
%         plot(sb(zc,:), [1 1]*(dmean(zc)-dci(zc))); ...
%         plot(sb(zc,:), [1 1]*(dmean(zc)+dci(zc)))];
%     set(hm{zc}, 'Color', colmark, 'LineWidth', 2);
end

if (lx>1)
    set(gca, 'XTick', xt);
end
% v = axis;
dy = (max(max(data)) - min(min(data)))/50; %(v(4)-v(3))/50;

%% Pair lines

if FlPaired
    ht = zeros(lx/2,1);
    fprintf('\n');
    
    for z = 1:2:lx,
        zi = (z+1)/2;
        sx = [xtd(:,z)'; xtd(:,z+1)'];
        sy = [data(:,z)'; data(:,z+1)'];
%         sx=[get(hd{z},'XData'); get(hd{z+1},'XData')];
%         sy=[get(hd{z},'YData'); get(hd{z+1},'YData')];
        
        [h,p,stat] = ttest(sy(1,:),sy(2,:));
        if isnan(p), p = 1;end
        fprintf('Paired t-test %i: H = %i, p = %5.1e, n = %i',zi,h,p,size(sy,2));
        
        idx = max(find(p<PLevel)); % 
        
        y0 = max(dmaxs(z:z+1));
        hll = plot([xt(z) xt(z) xt(z+1) xt(z+1)], ...
            y0+dy*[2 4 4 2],'k-','LineWidth',1);
        hold on;

        sidx = ~isnan(sy(1,:)) & ~isnan(sy(2,:));
        vabs = sy(2,sidx)-sy(1,sidx);
        if ~FlDLabel % label with significance
            s =  PSymbol{idx};
        else
            vrel = 100*vabs./sy(1,sidx);
            s = sprintf('%+3.0f%%',mean(vrel));
            s = strrep(s,' ','');
        end
        plot(mean(xt(z:z+1)), y0+dy*10, 'wo');

        ht(zi) = text(mean(xt(z:z+1)), y0+dy*4.2, s, ...
            'HorizontalAlignment','center', ...
            'VerticalAlignment','bottom');
        fprintf(', D = %5.3e +/- %5.3e\n',nanmean(vabs),nanstd(vabs));

        if FlPLines
            plot(sx,sy,'-','Color',coldata+.5);
        end
    end
end

%% Plot data and stats

for zc = 1:lx
    hd{zc} = scatter(xtd(:,zc), data(:,zc), 'o');
    set(hd{zc}, 'MarkerFaceColor', coldata, 'MarkerEdgeColor', coldata);
    hold on
    
    hm{zc} = [plot(lb(zc,:), [1 1]*dmean(zc)); ...
        plot(sb(zc,:), [1 1]*(dmean(zc)-dci(zc))); ...
        plot(sb(zc,:), [1 1]*(dmean(zc)+dci(zc)))];
    set(hm{zc}, 'Color', colmark, 'LineWidth', 2);
end


%% Output arguments

varargout{1} = xt;
varargout{2} = hd; % handles to data points
varargout{3} = hm; % handles to stats marker lines
varargout{4} = ht;
varargout{5} = hll;

return
