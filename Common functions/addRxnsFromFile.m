
function newmodel=addRxnsFromFile(model,file)


newmodel=model;

rxnstoadd=readtable(file,'Sheet','RxnsToAdd');
nr=height(rxnstoadd);
for z=1:nr
    name=rxnstoadd.rxnName{z};
    mets=rxnstoadd.met(z);
    lb=rxnstoadd.lb(z);
    ub=rxnstoadd.ub(z);
    ss=rxnstoadd.subsystem{z};
    stoich=str2num(rxnstoadd.stoich{z});
    
    mets=strrep(mets,'{','');
    mets=strrep(mets,'}','');
    mets=strrep(mets,'''','');
    mets=strsplit(mets{:},',');
    
    newmodel = addReaction(newmodel,name,mets,stoich,true,...
    lb,ub,0,ss,'',{},{},true);
end