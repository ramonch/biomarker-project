function testatp=testATPYields(model, atptestfile)

% a function returning the atp yields for carbon sources described in the
% atp test file under aerobic or anaerobic conditions

modelClosed = model;
modelexchanges=identifyAllExchangesRxns(modelClosed);
% biomass rxn
BM= (find(~cellfun(@isempty,strfind(lower(modelClosed.mets), 'bioma'))));
% add reaction demand atp
[modelClosed, rxnIDexists] = addReaction(modelClosed, 'DM_atp_c_', 'h2o[c] + atp[c] -> adp[c] + h[c] + pi[c] ');
if length(rxnIDexists) > 0

modelClosed.rxns{rxnIDexists} = 'DM_atp_c_'; % rename reaction in case that it exists already

end


modelClosed.lb(modelexchanges)=0;
modelClosed.ub(modelexchanges) = 1000;
modelClosed = changeObjective(modelClosed, 'DM_atp_c_');

testatp=readtable(atptestfile);
testatpids=findRxnIDs(model,testatp.carbon_source);
testatp=testatp(testatpids~=0,:);
nTest=height(testatp);


for z=1:nTest
    carbonSource=testatp.carbon_source(z);
    oxygen=testatp.Oxygen(z);
    modelClosedz = modelClosed;
    if strcmp(oxygen,'Aerobic')
        modelClosedz.lb(find(ismember(modelClosedz.rxns, 'EX_o2(e)'))) = -1000;
    end
    modelClosed.lb(find(ismember(modelClosedz.rxns, 'EX_h2o(e)'))) = -1000;
    modelClosedz.ub(find(ismember(modelClosedz.rxns, 'EX_h2o(e)'))) = 1000;
    modelClosedz.ub(find(ismember(modelClosedz.rxns, 'EX_co2(e)'))) = 1000;
    modelClosedz.lb(find(ismember(modelClosedz.rxns, carbonSource))) = -1;
    modelClosedz.ub(find(ismember(modelClosedz.rxns, carbonSource))) = -1;
    FBA = optimizeCbModel(modelClosedz, 'max');
    testatp.predictedYield(z)=FBA.obj;
end