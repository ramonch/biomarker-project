function modelexchanges=identifyAllExchangesRxns(modelClosed)
% function that identifies all model exchanges including sink and demand
% reactions , also uses the stoichiometry to help 
nRxns=length(modelClosed.rxns);
modelexchanges=zeros(nRxns,4);
modelexchanges(:,1) = startsWith(modelClosed.rxns,'EX_','IgnoreCase',true);
modelexchanges(:,2) = startsWith(modelClosed.rxns,'DM_','IgnoreCase',true);
modelexchanges(:,3) = startsWith(modelClosed.rxns,'sink_','IgnoreCase',true);
modelexchanges(:,4) = (full((sum(abs(modelClosed.S)==1, 1)==1) & (sum(modelClosed.S~=0) == 1)))';

modelexchanges=sum(modelexchanges,2)>0;
