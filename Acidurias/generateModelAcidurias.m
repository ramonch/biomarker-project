function [human,humanmod,genesTable,humanred,center]=generateModelAcidurias(modelcase,options)

filemodelcase=['modelcases/modelcase' num2str(modelcase) '.mat'];
if exist(filemodelcase,'file')>0
    load(filemodelcase)
else

    switch modelcase
        case 1 
            % model r204 downloaded from vmh directly version from 11 may
            % 2015
            % use the same constraints 
            model=load('Recon2.v04.mat');
            human=model.modelR204;
            human.lb(strmatch('EX_',human.rxns))=-1;
            human.ub(strmatch('EX_',human.rxns))=1000;
        case 2
            model=load('Recon2.v04.mat');
            human=model.modelR204;
            filerxns ='../Data publications/rxnsToAdd.xlsx';
            human=addRxnsFromFile(human,filerxns);
            human.lb(strmatch('EX_',human.rxns))=-1;
            human.ub(strmatch('EX_',human.rxns))=1000;
        case 3
            load('../Models/Recon22.mat')
            %Replace the maximum bounds by finite value
            human.lb(human.lb==-Inf)=-1000;
            human.ub(human.ub==Inf)=1000;
            % change the lowerbound of the reaction r0399 (was able to produce
            % phenylalanine from tyrosine
            zz=findRxnIDs(human,'r0399');
            human.lb(zz)=0;
            
            % convert the genes to NCBI entrez gene
            conversion=readtable('../Models/genesConversionRecon22.xlsx','Sheet','genes',...
            'ReadVariableNames',false);
            conversion.NCBIgene=strtrim(cellstr(strcat(num2str(conversion.Var2),'.1')));
            human.HGNC=human.genes;
            human.genes=conversion.NCBIgene;
            
        case 4
            load('../Models/recon1_eytan_ruppin.mat')
            %Replace the maximum bounds by finite value
            human=new_model;
            human.lb(human.lb==-Inf)=-1000;
            human.ub(human.ub==Inf)=1000;
            
            % format model
            
            human.rxns=strrep(human.rxns,'_LPAREN','');
            human.rxns=strrep(human.rxns,'RPAREN_','');
            human.rxns=strrep(human.rxns,'DASH_','');
            human.rxns=strrep(human.rxns,'BIOMASS','biomass_reaction');
            human.rxns(strcmp(human.rxns,'GND'))={'GNDc'};
            human.mets=strrep(human.mets,'DASH_','');
            
            % add reactions 
            
            %glycogen synthesis
            formula='-> ggn[c]';
            human = addReaction(human,'r1402','reactionFormula',formula,...
            'lowerBound',0,'upperBound',1000,'subsystem','Glycogen synthesis');

    end

    %% Update model 
    excelfile='acidurias_genes.xlsx';
    humanmod=updateModelAccordingtoFile(human, excelfile);
    rxnsAdd=readtable(excelfile,'Sheet','RxnsToAdd');
    genesTable=readtable(excelfile,'Sheet','Genes');
    rxnsGeneRulesChange=readtable(excelfile,'Sheet','changeRxnGeneRules');

    % update rules 
    % first generate the rules 
    rule=cellfun(@(c) strsplit(c,' '),rxnsAdd.Gene,'uni',0);

    for i=1:length(rule)
        zr=rule{i};
        if ~isempty(zr{1})
            [lia,locb]=ismember(zr,genesTable.GeneName);
            zr(lia)=cellstr(strcat(num2str(genesTable.GeneNCBIId(locb(lia))),'.1'));
            if length(zr)>1
                rxnsAdd.rule(i)={strjoin(zr,' ')};
            else
                rxnsAdd.rule(i)=zr;
            end
        end
    end 


    humanmod=updateReactionRules(humanmod,rxnsAdd.rxnName,rxnsAdd.rule);
    humanmod=updateReactionRules(humanmod,rxnsGeneRulesChange.RxnName,rxnsGeneRulesChange.Rule);

    %% see whether the added reactions are blocked 
    rxnsaddids=findRxnIDs(humanmod, rxnsAdd.rxnName);
    [minFlux,maxFlux] = fluxVariability_cr(humanmod,rxnsAdd.rxnName,0);

    blockedRxns=find(abs(maxFlux-minFlux)==0);
    fprintf('The added reactions %s are blocked\n', rxnsAdd.rxnName{blockedRxns});
end

%%

if options.flreduceModel && ~exist('humanred','var')
    fprintf('looking at the blocked reactions ...\n')
    blockedRxn=findBlockedReaction(humanmod);
    humanred=removeRxnsBiomarker(humanmod,blockedRxn);
end

if options.computeCenter && ~exist('center','var')
    fprintf('computing the center of the polytope...\n')
    % need to check the redundant constraints 
    humanred.lb(humanred.c==1)=1;
    center=findMVECenter(humanred);
    nRxns=length(humanred.rxns);

    %check that the center fulfils the steady-state equation
    norm(humanred.S*center)
    % check that the centers fulfills the inequalities 
    checkub=sum(center<=humanred.ub)==nRxns;
    checklb=sum(center>=humanred.lb)==nRxns;

    % check the value through the growth
    center(humanred.c==1)
    center(startsWith(humanred.rxns,'DM_atp'))
end


if options.forceSavingModel || exist(filemodelcase,'file')==0
    fprintf('Saving model...\n')
    save(filemodelcase)
end