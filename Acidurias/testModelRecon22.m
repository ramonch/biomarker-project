
clear
close all

%% load models
load('../Models/Recon22.mat')

%% Changes specific to recon2.2
%Replace the maximum bounds by finite value

human.lb(human.lb==-Inf)=-1000;
human.ub(human.ub==Inf)=1000;

% change the lowerbound of the reaction r0399 (was able to produce
% phenylalanine from tyrosine

zz=findRxnIDs(human,'r0399');
human.lb(zz)=0;
%% Update gene format
conversion=readtable('../Models/genesConversionRecon22.xlsx','Sheet','genes',...
    'ReadVariableNames',false);
conversion.NCBIgene=strtrim(cellstr(strcat(num2str(conversion.Var2),'.1')));
human.genes=conversion.NCBIgene;

%% Compute ATP yields
nRxns=length(human.rxns);
atptestfile='testModels/test_atpyields.xlsx';

testatp=testATPYields(human, atptestfile);

%% Check the growth rate 

medium=checkGrowthInDifferentMediaCondtions(human);


%
% what is the growth rate as a function of the glucose uptake rate 
ioRxn=findRxnIDs(human,{'EX_glc(e)','biomass_reaction','EX_o2(e)','EX_gln_L(e)',...
    'EX_lac_L(e)'});
labels={'Glucose uptake','Biomass','O2 uptake','Glutamine uptake','Lactate secretion'};

gur=0:25;
iorates=zeros(length(ioRxn),length(gur));
ioratesmin=zeros(length(ioRxn),length(gur));
ioratesmax=zeros(length(ioRxn),length(gur));

nTest=length(gur);
for z=1:nTest
    zmod=human;
    zmod.lb(ioRxn(1))=-gur(z);
    zmod.ub(ioRxn(1))=-gur(z);
    fba=optimizeCbModel(zmod);
    [ratemin,ratemax]=fluxVariability(zmod,100,'max',human.rxns(ioRxn));
    iorates(:,z)=fba.x(ioRxn);
    ioratesmin(:,z)=ratemin;
    ioratesmax(:,z)=ratemax;

end
%% plot

figure(1)
subplot(2,2,1)
ylabel('Biomass')
plot(gur, iorates(2,:))

subplot(2,2,2)
sp=3;
ylabel(labels(sp))
plot(gur,-ioratesmin(sp,:))
hold on 
plot(gur,-ioratesmax(sp,:))

subplot(2,2,3)
sp=4;
ylabel(labels(sp))
plot(gur,-ioratesmin(sp,:))
hold on 
plot(gur,-ioratesmax(sp,:))

subplot(2,2,4)
sp=5;
ylabel(labels(sp))
plot(gur,ioratesmin(sp,:))
hold on 
plot(gur,ioratesmax(sp,:))






%% Display distribution 
displayFluxDistributionMinerva(human,fba, 'DMEM medium')


%% Flux though PDHm 

pdhm=findRxnIDs(human, 'PDHm');

[minpdh,maxpdh]=fluxVariability(human,100,'max',{'PDHm'})


