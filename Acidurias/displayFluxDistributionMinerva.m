function serverResponse=displayFluxDistributionMinerva(model,fluxdistrib, fluxdistribname,color)

if nargin <4
    color='#6617B5';
end


load('minerva.mat')
minerva.minervaURL = 'http://www.vmh.life/minerva/galaxy.xhtml';
minerva.map = 'ReconMap-2.01';
minerva.login = 'char.ramon';
minerva.password = 'char.ramon1';
minerva.googleLicenseConsent = 'true';

serverResponse = buildFluxDistLayout(minerva, model, fluxdistrib, fluxdistribname,[],color);


