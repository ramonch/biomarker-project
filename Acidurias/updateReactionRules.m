function newmodel=updateReactionRules(model,listRxns,rule)
newmodel=model;
rxnsids=findRxnIDs(model,listRxns);

for i=1:length(rule)
    if rxnsids(i)~=0
        newmodel=changeGeneAssociation(newmodel,listRxns{i},rule{i});
    end
end
end