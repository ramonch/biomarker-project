%% Try to reduce the recon22 model

humanredConfident=humanred;


rxnsZeroConfidence=humanred.rxns(humanred.rxnConfidenceScores==0);
cnt=0;

while ~isempty(rxnsZeroConfidence)
    cnt=cnt+1;
    % potential model
    zmod=humanredConfident;
    zrxn=rxnsZeroConfidence(1);
    zrxnid=findRxnIDs(zmod,zrxn);
    zmod.lb(zrxnid)=0;
    zmod.ub(zrxnid)=0;
    
    zblockedRxns = findBlockedReaction(zmod);
    zblockedRxnsids=findRxnIDs(zmod,zblockedRxns);
    blockCS=zmod.rxnConfidenceScores(zblockedRxnsids);
    fprintf('The reaction %s leads to %i blocked rxns \n',zrxn{:},length(zblockedRxns))
    fprintf('with a sum confidence score: %i \n',sum(blockCS))

    if sum(blockCS)==0
        fprintf('We remove  the blocked rxns \n')
        humanredConfident=removeRxnsBiomarker(humanredConfident,zblockedRxns);
        fprintf('The resulting models has %i reactions \n',length(humanredConfident.rxns))
        rxnsZeroConfidence=setdiff(rxnsZeroConfidence,zblockedRxns);
    else
        fprintf('We do not block the reaction %s \n',zrxn{:})
        rxnsZeroConfidence=setdiff(rxnsZeroConfidence,zrxn{:});
    end
    
    fprintf('There are %i reactions with zero confidence left to study \n',length(rxnsZeroConfidence))
    
end










