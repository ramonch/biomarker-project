function newmodel=updateModelAccordingtoFile(model, file)

newmodel=model;
genes=readtable(file,'Sheet','Genes');
rxnsDelete=readtable(file,'Sheet','RxnsToDelete');
metsChange=readtable(file,'Sheet','ChangeMetName');
metsAdd=readtable(file,'Sheet','MetsToAdd');
rxnsAdd=readtable(file,'Sheet','RxnsToAdd');
%%

% first delete reactions 
newmodel=removeRxns(newmodel,rxnsDelete.RxnName);


% How many genes without GPRs
% sprintf('There are %i genes without GPRs \n',sum(sum(newmodel.rxnGeneMat, 1) == 0))
% storeGPR = newmodel.grRules;
% newmodel.rxnGeneMat = [];
% newmodel.genes = [];
% 
% for i = 1 : length(newmodel.rxns)
%     newmodel = changeGeneAssociation(newmodel, newmodel.rxns{i}, storeGPR{i});
% end
% 
% sprintf('There are %i genes left without GPRs \n',sum(sum(newmodel.rxnGeneMat, 1) == 0))

% change mets name 

for i=1:length(metsChange.OldName)
    zmetid= findMetIDs(newmodel, metsChange.OldName{i});
    if zmetid==0
        newmodel = addMetabolite(newmodel, metsChange.NewName{i}, metsChange.Descrition{i});
    else
        newmodel.mets(zmetid)=metsChange.NewName(i);
    end
end

% add Metabolites 

for i=1:length(metsAdd.Mets)
    metid=findMetIDs(newmodel, metsAdd.Mets{i});
    if metid ==0
        newmodel = addMetabolite(newmodel, metsAdd.Mets{i}, metsAdd.Name{i});
    end
end

% add Reactions 

newmodel=addRxnsFromFile(newmodel,file);



