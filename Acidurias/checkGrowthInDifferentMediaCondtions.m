function medium=checkGrowthInDifferentMediaCondtions(model,sheet)

medium=readtable('medium/dmem.xlsx','Sheet',sheet);

% set all exchanges lower bounds to zero
exchanges=identifyAllExchangesRxns(model);
model.lb(exchanges)=0;

% set the media to DMEM
dmemids=findRxnIDs(model,medium.Reaction_name);

medium=medium(dmemids~=0,:);
dmemids=dmemids(dmemids~=0);

model.lb(dmemids)=medium.LB;
model.ub(dmemids)=medium.UB;

%check the GR in this condition
fba=optimizeCbModel(model);
gr=fba.obj

nComponentsMedium=height(medium);

for z =1:nComponentsMedium
    zmod=model;
    zr=dmemids(z);
    zmod.lb(zr)=0;
    zmod.ub(zr)=0;
    
    fba=optimizeCbModel(zmod);
    medium.gr(z)=fba.obj;
end