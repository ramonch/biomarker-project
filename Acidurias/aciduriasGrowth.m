clear
close all

%% load models
modelcase=4;
options.flreduceModel=1;
options.computeCenter=0;
options.forceSavingModel=0;

[human,humanmod,genesTable,humanred]=generateModelAcidurias(modelcase,options);
%% model to consider
mediumDef='mem';
consmod=humanmod;


%% Test with ramirez publication 
testATP=testATPYields(consmod, 'testModels/test_atpyields.xlsx');

% test the growth in different media conditions 

medium=checkGrowthInDifferentMediaCondtions(consmod,mediumDef);

%% Set the edium to the defined medium

dmem=readtable('medium/dmem.xlsx','Sheet',mediumDef);

% set all exchanges lower bounds to zero
exchanges=identifyAllExchangesRxns(consmod);
consmod.lb(exchanges)=0;

% set the media to DMEM
dmemids=findRxnIDs(consmod,dmem.Reaction_name);
blockeddmem=dmemids~=0;
consmod.lb(dmemids(blockeddmem))=dmem.LB(blockeddmem);
consmod.ub(dmemids(blockeddmem))=dmem.UB(blockeddmem);

% 
fba=optimizeCbModel(consmod);
[consmod.rxns(dmemids(blockeddmem)),num2cell(fba.full(dmemids(blockeddmem)))]


% check that there is no growth when no clucose and no glutamine
shutdownrxn={'EX_glc(e)','EX_gln_L(e)'};
shutdownrxnId=findRxnIDs(consmod,shutdownrxn);
ztest=consmod;
ztest.lb(shutdownrxnId)=0;
ztest.ub(shutdownrxnId)=0;

fba=optimizeCbModel(ztest,'max',0,false);
%% characterization of the medium 

ratioCarbAA=sum(-dmem.LB(strcmp(dmem.Type,'Carbohydrate')))/...
    sum(-dmem.LB(strcmp(dmem.Type,'AA')));

ratioGlnGlc=-dmem.LB(strcmp(dmem.Reaction_name,'EX_gln_L(e)'))/...
    -dmem.LB(strcmp(dmem.Reaction_name,'EX_glc(e)'));
    





%% genes loading

genes=table;
genes.Id=strtrim(strcat(cellstr(num2str(genesTable.GeneNCBIId)),'.1'));
genes.idm=findGeneIDs(consmod,genes.Id);
genes.geneName=genesTable.GeneName;

% reduce to genes that are already in the model
genes=genes(genes.idm~=0,:);

for ig=1:size(genes,1)                                                                        
    g=genes.idm(ig);
    [rxnTot, constrainRxn,~] = detectAffectedRxnsByGene(consmod,genes.Id(ig));
    genes.nbrxn(ig)=sum(constrainRxn);
    genes.rxns{ig}=consmod.rxns(rxnTot(constrainRxn));
end
ng=height(genes);
%%
% what is the growth rate as a function of the glucose uptake rate 
interestingRxn={'EX_glc(e)','biomass_reaction','EX_o2(e)','EX_gln_L(e)',...
    'EX_lac_L(e)','EX_co2(e)','MMMm','PDHm','PGI','GNDc','SUCOAS1m','AKGDm','SUCD1m',...
    'ICDHxm','GLUDxm','ACACT10m','CSm','ACCOACm','GLYATm','ACACT1rm','ASPNATm',...
    'CSNATm'};
ioRxn=findRxnIDs(consmod,interestingRxn);

rate=0:10;
mediumCompChangeRate={'EX_glc(e)','EX_gln_L(e)','EX_Lcystin(e)'};
mediumCompChangeRateIds=findRxnIDs(consmod,mediumCompChangeRate);

compchange=3;

iorates=zeros(length(ioRxn),length(rate));
ioratesmin=zeros(length(ioRxn),length(rate));
ioratesmax=zeros(length(ioRxn),length(rate));

nTest=length(rate);
for z= 1:nTest
    zmod=consmod;
    zmod.lb(mediumCompChangeRateIds(compchange))=-rate(z);
    zmod.ub(mediumCompChangeRateIds(compchange))=-rate(z);
    fba=optimizeCbModel(zmod);
    [ratemin,ratemax]=fluxVariability_cr(zmod,zmod.rxns(ioRxn),100);
    iorates(:,z)=fba.x(ioRxn);
    ioratesmin(:,z)=ratemin;
    ioratesmax(:,z)=ratemax;

end
%% plot

figure(compchange);clf;

icompnotchange=find(~strcmp(mediumCompChangeRate(compchange),interestingRxn));

for sp=1:length(interestingRxn)-1
zcomp=icompnotchange(sp);
subplot(5,5,sp)
plot(rate,ioratesmin(zcomp,:))
hold on 
plot(rate,ioratesmax(zcomp,:))
ylabel(interestingRxn(zcomp))
end
title(mediumCompChangeRate(compchange))


%% 
ig=1;
rxn=genes.rxns{ig};
rxnId=findRxnIDs(consmod,rxn);
humanko=consmod;
humanko.lb(rxnId)=0;
humanko.ub(rxnId)=0;
humanhealthy=consmod;
humanhealthy.lb(rxnId)=0.01;

%optimizeCbModel(humanhealthy)
wt=optimizeCbModel(consmod);
ko=optimizeCbModel(humanko);
% first check whether the function at stake has a flux in the original
% model
[ratemin,ratemax]=fluxVariability_cr(consmod,rxn,100)

%%
% check the growth ratio between wild type and mutant for different carbon
% sources

carbonsource=readtable('medium/carbon_sources.xlsx');
carbonsource.ids=findRxnIDs(consmod,carbonsource.rxn);
ncs=height(carbonsource);
csUr=[1,10];
nCsUr=length(csUr);
glutUr=[0,1];

cond=combvec(1:ncs,csUr,glutUr);
conditions=table;
conditions.carbonSourceRxn=carbonsource.rxn(cond(1,:));
conditions.carbonSourceID=carbonsource.ids(cond(1,:));
conditions.carbonSourceLevel=cond(2,:)';
conditions.glutamineLevel=cond(3,:)';
nCond=height(conditions);
exGln=findRxnIDs(consmod,'EX_gln_L(e)');

for i=1:nCond

    zwt=consmod;
    zko=consmod;
    zko.lb(rxnId)=0;
    zko.ub(rxnId)=0;

    % set all uptake all cc to 0
    zwt.lb(carbonsource.ids)=0;
    zko.lb(carbonsource.ids)=0;

    %set the carbon source uptake 
    zwt.lb(conditions.carbonSourceID(i))=-conditions.carbonSourceLevel(i);
    zko.lb(conditions.carbonSourceID(i))=-conditions.carbonSourceLevel(i);
    
    zwt.lb(exGln)=-conditions.glutamineLevel(i);
    zko.lb(exGln)=-conditions.glutamineLevel(i);
    %compute the gr
    wtsol=optimizeCbModel(zwt);
    kosol=optimizeCbModel(zko);
    conditions.grWT(i)=wtsol.obj;
    conditions.grKO(i)=kosol.obj;
    conditions.grRatio(i)=conditions.grKO(i)/conditions.grWT(i);

end





%% Display distribution 
%displayFluxDistributionMinerva(ztest,fba, 'DMEM medium')


%% Try to find the optimal carbon source to test 
addpath(genpath('../SensitivityWithConstraints'))
fba=optimizeCbModel(consmod);
pertRxn=findRxnIDs(consmod,'EX_gln_L(e)');
pertMagnitude=-1;
nRxns=length(consmod.rxns);

exchangesid=find(exchanges);
nExchanges=length(exchangesid);
sensitivityExchanges=nan(nRxns,nExchanges);
for i=1:nExchanges
    i
    pertRxn=exchangesid(i);
    zmod=consmod;
    zmod.lb(pertRxn)=pertMagnitude+zmod.lb(pertRxn);
    sensitivity = sensitivityWithSoftConstraints(zmod, pertRxn, fba.x,pertMagnitude,...
        true,'biomass_reaction');
    if ~isempty(sensitivity.x)
        sensitivityExchanges(:,i)=sensitivity.x;
    end
end



irxn=findRxnIDs(consmod,'MMMm');
potentialEx=consmod.rxns(exchangesid(sensitivityExchanges(irxn,:)>0.01));
potentialEx=potentialEx(startsWith(potentialEx,'EX_'));
nPotentialEx=length(potentialEx);
pertMagnitude=-1;
glcex=findRxnIDs(consmod,'EX_glc(e)');
gur=[-1,-10];
grpotentialEx=zeros(nPotentialEx,2,length(gur));

for zgur=1:length(gur)
    for i=1:nPotentialEx
        zmod=consmod;
        zmod.lb(glcex)=gur(zgur);
        zrxn=findRxnIDs(consmod,potentialEx(i));
        zmod.lb(zrxn)=pertMagnitude+zmod.lb(zrxn);
        %zmod.ub(pertRxn)=pertMagnitude+zmod.lb(pertRxn);

        gr=optimizeCbModel(zmod);
        grpotentialEx(i,1,zgur)=gr.obj;

        zmod.lb(irxn)=0;
        zmod.ub(irxn)=0;

        gr2=optimizeCbModel(zmod);
        grpotentialEx(i,2,zgur)=gr2.obj;

    end
end
ratioGr(:,1)=grpotentialEx(:,2,1)./grpotentialEx(:,1,1);
ratioGr(:,2)=grpotentialEx(:,2,2)./grpotentialEx(:,1,2);

[~,zsort]=sort(ratioGr,'ascend');

[potentialEx(zsort),num2cell(ratioGr(zsort))]

%%
[interestingRxn',num2cell(sensitivity.x(ioRxn)),num2cell(fba.x(ioRxn))]

resultingFlux=sensitivity.x+fba.x

sensitivity.x(pertRxn)
