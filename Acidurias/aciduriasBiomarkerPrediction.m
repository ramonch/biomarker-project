clear
close all

%% load models
modelcase=2;
options.flreduceModel=1;
options.computeCenter=1;

[human,humanmod,humanred,center]=generateModelAcidurias(modelcase,options);




%% genes we are interested in

genes=table;
genes.Id=strtrim(strcat(cellstr(num2str(genesTable.GeneNCBIId)),'.1'));
genes.idm=findGeneIDs(humanred,genes.Id);
genes.geneName=genesTable.GeneName;

% reduce to genes that are already in the model
genes=genes(genes.idm~=0,:);


%
for ig=1:size(genes,1)                                                                        
    g=genes.idm(ig);
    [rxnTot, constrainRxn,~] = detectAffectedRxnsByGene(humanred,genes.Id(ig));
    genes.nbrxn(ig)=sum(constrainRxn);
    genes.rxns{ig}=humanred.rxns(rxnTot(constrainRxn));
end
ng=height(genes);


%% Defining the biomarkers
exbool=startsWith(humanred.rxns,'EX_');
biomidxred=find(exbool);
biomlabelsred=humanred.rxnNames(biomidxred);

biomlabelsred=strrep(biomlabelsred,'Exchange','');
biomlabelsred=strrep(biomlabelsred,'of','');
biomlabelsred=strrep(biomlabelsred,'exchange','');
biomlabelsred=strrep(biomlabelsred,'reaction','');
biomlabelsred=strrep(biomlabelsred,'for','');

%% predict biomarkers sensitivity with constraints

version='softconstraints';
[listBiomarkWithCons,pertNorm]=predictBiomarkersSensWithConstraintsGenes(humanred,genes, ...
   center,biomidxred,biomlabelsred,version);


pertNorm2=zeros(size(pertNorm));
for i=1:ng
    pertNorm2(i)=norm(listBiomarkWithCons(i,:));
end
% normalizing with the effect of the perturbation on the biomarker
listBiomarkWithConsNorm=listBiomarkWithCons./repmat(pertNorm2,1,size(listBiomarkWithCons,2));

%% Plot heat map
thresh=0.10;
ig=1:height(genes);
data=double(abs(listBiomarkWithConsNorm(ig,:))>thresh);

leftbiom=~all(data==0);

figure
HM=HeatMap(flipud(sign(listBiomarkWithConsNorm(:,leftbiom)).*data(:,leftbiom)), 'ColumnLabels',biomlabelsred(leftbiom),...
    'ColorMap','redbluecmap','RowLabels',flipud(genes.geneName(ig)),'Annotate',true);
plot(HM)
%% Investigating specific genes 

%eg: MTHFR
ig=9;
exhcys=find(contains(humanred.rxns(exbool),'EX_met'));
listBiomarkWithConsNorm(ig,exhcys)
