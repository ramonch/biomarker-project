function [U, R, V] = urvdelete(U, R, V, i)
%[U,R,V] = URVDELETE(U,R,V,I)  Remove I-th row of URV-decomposition.
%
%   [U,R,V] = URVDELETE(U,R,V,I) removes the row indexed by I from the URV
%   factorization U*R*V'. If the matrix U*R*V' is an m-by-n matrix then the
%   result is an (m-1)-by-n matrix.
%
%   Deleting a row from a URV decomposition is an O(m^2+r*(m+n)) process.
%
%   See also URV.

% problem dimension
[m,n] = size(R);

% rank estimate
%r = nnz(abs(diag(R))>1e-12); % very crude estimate
r = nnz(abs(diag(R))>abs(R(1,1)*eps*max(m,n)));

% row permutation to bring i-th row to index 1
U = U([i 1:i-1 i+1:m],:);

% zero out first row of U
for k = m-1:-1:1
    F = planerot(U(1,k:k+1)');
    U(:,k:k+1) = U(:,k:k+1)*F';
    R(k:k+1,:) = F*R(k:k+1,:);
end

% remove first row
U = U(2:m,2:m);
R = R(2:m,:);
    
% check if downdated matrix lost rank
%l = find(abs(diag(R))<1e-12, 1) % crude estimate
l = find(abs(diag(R))<abs(R(1,1)*eps*max(m,n)), 1);
if isempty(l)
    l = m;
end

% eventually make decomposition complete again
if l <= r
    for k = l+1:r
        F = planerot(R(k-1:k,k));
        U(:,k-1:k) = U(:,k-1:k)*F';
        R(k-1:k,:) = F*R(k-1:k,:);
    end
    r = r-1;
    for k = r:-1:1
        G = planerot(R(k,[k r+1])');
        R(:,[k r+1]) = R(:,[k r+1])*G';
        V(:,[k r+1]) = V(:,[k r+1])*G';
    end
end

