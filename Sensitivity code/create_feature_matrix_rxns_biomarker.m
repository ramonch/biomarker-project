function FM=create_feature_matrix_rxns_biomarker(model,rxnIds,tolerance)
FM=zeros(length(rxnIds),length(model.rxns));
%[new_model,~]=normalizeStoichMatrix(model);
new_model=model;
Stoich=full(new_model.S);
[U, R, V] = svd(Stoich');
N{1}=U;
N{2}=R;
N{3}=V;
 parfor i=1:length(rxnIds)
     i
%      sens = l2_sensitivity_crv2_norm1(N,rxnIds(i),... 
%      [],false,tolerance);     
     sens = l2_sensitivity_analysis(Stoich,rxnIds(i),... 
     [],false,tolerance); 
     FM(i,:)=sens;
 end
 
end