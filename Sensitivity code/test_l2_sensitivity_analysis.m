clear
close all

%%
testcase=2;
load(['../initialization files/testcase' num2str(testcase) '_init.mat'])



%% Reduce model and SM Matrix 
leftrxn=find(~isnan(SM(:,1)));
SMred=SM(leftrxn,leftrxn);
humanred=human;
humanred.rxns=human.rxns(leftrxn);
humanred.rules=human.rules(leftrxn);
humanred.S=human.S(:,leftrxn);

if isfield(human,'rxnGeneMat')
    humanred.rxnGeneMat=human.rxnGeneMat(leftrxn,:);
end

[~, biomidxred]=ismember(biomidx,leftrxn);


%%

Names={'DLD','DBT','BCKDHA','BCKDHB','OGDH'};
Id={'1738.1','1629.1','593.1','594.1','4967.1'}';
g=table;
g.Genes=Names';
g.Id=Id;

% MSUD is supposed to inhibit via the ketoacids OGDH 
g.idm=findGeneIDs(humanred,g.Id);

for z=1:size(g,1)
    [zrxn,zconstrainrxn,~]=detectAffectedRxnsByGene(humanred,g.Id(z));
    g.affectedRxn{z}=zrxn(zconstrainrxn);
end


%%
rxn=g.affectedRxn{1};
N=full(humanred.S);
[U,E,V] = svd(N');
stoich={U,E,V};
%%
[sg,maxRes,~,delta,dirchange]=l2_sensitivity_analysis(stoich,rxn,[],true,tolerance);