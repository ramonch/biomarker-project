function [S,max_res,Z,delta,dirchange] = l2_sensitivity_analysis(N, I, vref, dirBool,tolerance)
%L2_SENSITIVITY   Compute L2-sensitivities to flux disturbances.
%
%   S = L2_SENSITIVITY(N,I) returns the absolute sensitivities of the
%   fluxes in the network described by the stoichiometric matrix N. The
%   fluxes with indices I are assumed to be disturbed
%   (i.e. S(I,1:k)==eye(k)).
%
%   S = L2_SENSITIVITY(N,I,VREF) computes the relative sensitivities to the
%   given reference flux VREF. That is, it minimizes the adjustments with
%   respect to the norm d'*diag(1./(VREF.^2))*d.
%
%   S = L2_SENSITIVITY(N,I,VREF,DIR) computes the absolute (or relative if
%   VREF is not empty) sensitivities for a disturbance in the given
%   direction DIR.
%
%   [S,MAX_RES] = L2_SENSITIVITY(...). If the index set I (or, if
%   specified, the direction DIR) is incompatible with the stoichiometric
%   matrix N, L2_SENSITIVITY returns an empty matrix for S and the magnitude
%   of the maximal residual in MAX_RES.
%
%   [S,MAX_RES,Z] = L2_SENSITIVITY(...) also returns a basis Z of feasible
%   disturbances. The columns of Z form an orthonormal basis. For example,
%   test directions for disturbances could be generated with Z*randn(r,1)
%   where size(Z,2)==r.
%
%   For performance reasons, a complete orthogonal decomposition (e.g. URV
%   decomposition or SVD) of the transposed stoichiometric matrix can be
%   passed to the routine directly. In this case, the first parameter is a
%   cell array {U, R, V} where U, V are orthonormal matrices, R is upper
%   triangular and the product U*R*V' = N'. Note that in this case, it is
%   not possible to compute relative sensitivities, i.e. the parameter VREF
%   must be empty.
%
%   Example:
%      S = l2_sensitivity(N,3)
%
%   computes the absolute sensitivities of the network with stoichoimetric
%   matrix N for a disturbance in reaction 3.
%
%      [U,E,V] = svd(N');
%      S = l2_sensitivity({U,E,V},[3;4],[],[1;1])
%
%   computes the sensitivities of the fluxes in a network with
%   stoichiometric matrix N to a disturbance in reactions 3 and 4.
%
%   See also URV, SVD.

% version 0.4.1 -- 07 Aug. 2009
dirchange=0;
if nargin >= 3 && ~isempty(vref)
    if length(vref) ~= size(N,2)
        error('Incompatible dimension of reference flux vector.');
    end
else
    if ~iscell(N)
        vref = ones(size(N,2),1);
    else 
        stoich=(N{1}*N{2}*N{3})';
        vref = ones(size(stoich,2),1);
    end
end

has_dir = false;
if nargin >= 4
    if dirBool==true
        has_dir = true;
        dir=ones(length(I),1);
        delta = dir(:)/norm(dir(:),2);
    else
        dir=1;
        delta=1;
    end
end

I = I(:);

if ~iscell(N)
    % problem dimensions
    [m, n] = size(N);
    k = length(I);

    % "free" indices
    Ic = setdiff((1:n)', I);
    
    Nk = N(:,I);

    % compute SVD of constraint matrix
    A = bsxfun(@times, vref(Ic), N(:,Ic)');
    if length(Ic) < m
        [U, R, V] = svd(A);
    else
        [U, R, V] = svd(A,0);
    end
else

    U = N{1};
    R = N{2};
    V = N{3};
    
    % problem dimensions
    [n,m] = size(R);
    k = length(I);

    % "free" indices
    Ic = setdiff((1:n)', I);

    Nk = V*R'*U(I,:)';
    
    Is = sort(I, 'descend');
    for i = 1:k
        [U, R, V] = urvdelete(U, R, V, Is(i));
    end
end

% estimate rank of N1
%tol = max(m,n)*max(abs(diag(R)))*1e-12;
tol=max(m,n)*max(abs(diag(R)))*tolerance;
r = nnz(abs(diag(R)) > tol);
Vn=V(:,r+1:end);
Vr=V(:,1:r);
Ur=U(:,1:r);
Un=U(:,r+1:end);

test =Vn'*Nk;
norm(test)
if norm(test)>tol
    if norm(test*delta)>tol
        if length(dir)>1
            disp('Had to change the dir expression, particular perturbation not feasible')
            size_test=size(test);
            beq=zeros(size_test(1),1);
            basis=null(test);
            x0=sum(basis,2);
            options=optimoptions('fmincon');
            options.ConstraintTolerance=10^(-12);
            delta
            [dir,fval,exitflag,output]=fmincon(@(x) sum(abs(x-1)),x0,[],[],test,beq,[],[],[],options);
            fprintf('%d \n',fval);
            delta=dir/norm(dir(:),2)
            dirchange=1;
        end
    else
        disp('This particular perturbation is feasible')
    end
end
if has_dir
    S = zeros(n,1);
    S(I) = delta;
    % solve (rank deficient) least squares problem
    S(Ic) = -U(:,1:r)*(R(1:r,1:r)'\(V(:,1:r)'*Nk*delta)); % assuming delta is absolute, not relative
    % compute residual
    res = V(:,r+1:m)'*(Nk*delta);
    size(V(:,r+1:m)');
    size(Nk);
    maximal_residual=max(max(abs(res)));
else
    S = zeros(n,k);
    S(I,1:k) = eye(k)*delta;
    % solve (rank deficient) least squares problem
    
    % does not work !!!
    S(Ic,:) = -U(:,1:r)*(R(1:r,1:r)'\(V(:,1:r)'*Nk*delta*diag(vref(I))));
    % compute residual
    res = V(:,r+1:m)'*Nk*delta;
end

% check if the set of indices I is compatible with steady 
% state constraints.
max_res = max(max(abs(res)));
% disp(sprintf('max_res=%g, tol=%g, smax=%g',max_res,tol*(1e2+norm(delta)),max(abs(diag(R)))));
if max_res > tol
    S(Ic,:) = nan;
    if nargout > 2
        if has_dir
            Z = zeros(length(I),1);
        else
            [U,E,V] = svd(V(:,r+1:m)'*Nk,0);
            % tol here is less strict because U is already inaccurate.
            % Maybe we should multiply only with 1e-8?
            tol = max(m-r,k)*max(abs(diag(E)))*1e-10;
            r = nnz(abs(diag(E)) > tol);
            Z = V(:,r+1:end);
        end
    end
else
    if nargout>2
        Z='maxres is okay';
    end
end
