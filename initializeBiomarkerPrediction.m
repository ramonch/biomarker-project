
if ~exist ('human','var')
    switch testcase
        case 1
            filename='publiSchlomi2009';
            load([folderModel 'recon1_eytan_ruppin.mat'])
            human=new_model;
            human=format_met_ruppinmodel(human);
        case 2
            filename='publiThiele2015';
            load([folderModel 'recon2v04.mat'])
            human=modelR204;
            i_incons=find((human.lb==0) .* (human.ub==0 ).* (human.rev==0));
            human.ub(i_incons)=1000;
        case 3
            filename='publiSchlomi2009';
            load([folderModel 'Recon1.mat'])
            human=model;
            human=format_met_recon1(human);
        case 4 
            filename='publiThiele2015';
            human=readCbModel([folderModel 'recon2_model.xml']);
            file=[folderGene filename '.xlsx'];
            constraints = readtable(file,'ReadVariableNames',true, ...
            'Sheet','Constraints' );
            [~,inTable]=ismember(human.rxns,constraints.Rid);
            human.lb=constraints.lb(inTable);
            human.ub=constraints.ub(inTable);
        case 5
            filename='publiThiele2015';
            human=readCbModel([folderModel 'recon2.xml']);
            file=[folderGene filename '.xlsx'];
            constraints = readtable(file,'ReadVariableNames',true, ...
            'Sheet','Constraints' );
            [~,inTable]=ismember(human.rxns,constraints.Rid);
            human.lb=constraints.lb(inTable);
            human.ub=constraints.ub(inTable);
        case 6
            human=readCbModel([folderModel 'iMyocyte2419_Cobra.xml']);
        case 7
            % model does not grow
            human=readCbModel([folderModel 'tissue-specific recon/heart_muscle_myocytes.xml']);
            filename='publiThiele2015';
            biomass=find(startsWith(human.rxns,'bio'));
            human.c(biomass)=1;
        case 8
            % model does not grow
            human=readCbModel([folderModel 'tissue-specific recon/skeletal_muscle_myocytes.xml']);
            filename='publiThiele2015';
            biomass=find(startsWith(human.rxns,'bio'));
            human.c(biomass)=1;
            selexc=findExcRxns(human);
            human.ub(selexc)=1000;
        case 9 
            filename='publiThiele2015';
            model=load('2013_Recon2/Recon-2.mat');
            %%% Be aware wrong GPR description
            human=model.modelRecon2beta121114;
            file=[folderGene filename '.xlsx'];
            human.lb(strmatch('EX_',human.rxns))=-1;
            human.ub(strmatch('EX_',human.rxns))=1000;
        case 10 
            filename='publiThiele2015';
            model=load('2013_Recon2/Recon-2.mat');
            %%% Be aware wrong GPR description
            human=model.modelRecon2beta121114;
            file=[folderGene filename '.xlsx'];
            filerxns ='../Data publications/rxnsToAdd.xlsx';
            human=addRxnsFromFile(human,filerxns);
            human.lb(strmatch('EX_',human.rxns))=-1;
            human.ub(strmatch('EX_',human.rxns))=1000;
        case 11 
            % model r204 downloaded from vmh directly version from 11 may
            % 2015
            % use the same constraints 
            filename='publiThiele2015';
            model=load('Recon2.v04.mat');
            human=model.modelR204;
            file=[folderGene filename '.xlsx'];
            human.lb(strmatch('EX_',human.rxns))=-1;
            human.ub(strmatch('EX_',human.rxns))=1000;
        case 12
            filename='publiThiele2015';
            model=load('Recon2.v04.mat');
            human=model.modelR204;
            file=[folderGene filename '.xlsx'];
            filerxns ='../Data publications/rxnsToAdd.xlsx';
            human=addRxnsFromFile(human,filerxns);
            human.lb(strmatch('EX_',human.rxns))=-1;
            human.ub(strmatch('EX_',human.rxns))=1000;
        case 13
            fprintf('testcase13 \n')
            model=load('Recon2.v04.mat');
            human=model.modelR204;
            filerxns ='../Data publications/rxnsToAddZamboni.xlsx';
            filename=filerxns;
            file=[folderGene filename '.xlsx'];
            human.lb(strmatch('EX_',human.rxns))=-1;
            human.ub(strmatch('EX_',human.rxns))=1000;
            humanmod=updateModelAccordingtoFile(human, filerxns);
            rxnsAdd=readtable(filerxns,'Sheet','RxnsToAdd');
            genesTable=readtable(filerxns,'Sheet','Genes');
            rxnsGeneRulesChange=readtable(filerxns,'Sheet','changeRxnGeneRules');

            % update rules 
            % first generate the rules 
            rule=cellfun(@(c) strsplit(c,' '),rxnsAdd.Gene,'uni',0);

            for i=1:length(rule)
                zr=rule{i};
                if ~isempty(zr{1})
                    [lia,locb]=ismember(zr,genesTable.GeneName);
                    zr(lia)=cellstr(strcat(num2str(genesTable.GeneNCBIId(locb(lia))),'.1'));
                    if length(zr)>1
                        rxnsAdd.rule(i)={strjoin(zr,' ')};
                    else
                        rxnsAdd.rule(i)=zr;
                    end
                end
            end 

            fprintf('Updating reaction rules \n')
            humanmod=updateReactionRules(humanmod,rxnsAdd.rxnName,rxnsAdd.rule);
            humanmod=updateReactionRules(humanmod,rxnsGeneRulesChange.RxnName,rxnsGeneRulesChange.Rule);

            p=genpath('/Users/ramonch/Documents/MATLAB/Charlotte_func/');
            addpath(p)
            % see whether the added reactions are blocked 
            rxnsaddids=findRxnIDs(humanmod, rxnsAdd.rxnName);
            [minFlux,maxFlux] = fluxVariability_cr(humanmod,rxnsAdd.rxnName,0);

            blockedRxns=find(abs(maxFlux-minFlux)==0);
            fprintf('The added reactions %s are blocked\n', rxnsAdd.rxnName{blockedRxns});
            human=humanmod;
            human=rmfield(human,'rxnConfidenceScores');
    end
end
%%
if ~exist('SM','var')
    SM=create_feature_matrix_rxns_biomarker(human, 1:length(human.rxns),tolerance);
end
if ~exist('genes','var') && flretrieveGenes
    file=[folderGene filename '.xlsx'];
    opts=detectImportOptions(file);
    opts = setvartype(opts,'Id','char');
    genes = readtable(file,opts,'ReadVariableNames',true, ...
        'Sheet','Genes' );
    genes.idm=findGeneIDs(human,genes.Id);

    for ig=1:size(genes,1)
        g=genes.idm(ig);
        [~, constrainRxn,~] = detectAffectedRxnsByGene(human,genes.Id(ig));
        genes.nbrxn(ig)=sum(constrainRxn);
    end
end


%%
if flreduceModel && ~exist('humanred','var')
    blockedRxn=findBlockedReaction(human);
    humanred=removeRxnsBiomarker(human,blockedRxn);
    SMred=create_feature_matrix_rxns_biomarker(humanred, 1:length(humanred.rxns),tolerance);
end
%%
if ~fluseAllExch
    biomark=readtable(file,'ReadVariableNames',true, ...
        'Sheet','Biomarkers');
    [biom,biomidx,biomfound]=findExcRxnFromMets(human,biomark.abbrev);
    biomark=biomark(biomfound,:);
    biomidx=biomidx(biomfound);
    biomlabels=biomark.abbrev;
    if exist('humanred','var')
        [biomred,biomidxred,biomfoundred]=findExcRxnFromMets(humanred,biomark.abbrev);
        biomarkred=biomark(biomfoundred,:);
        biomidxred=biomidxred(biomfoundred);
        biomlabelsred=biomarkred.abbrev;

    end
else
    biombool=findExcRxns(human);
    biomidx=find(biombool);
    biom=human.rxns(biomidx);
    biomlabels=human.rxnNames(biomidx);

    if exist('humanred','var')
        biomboolred=findExcRxns(humanred);
        biomidxred=find(biomboolred);
        biomred=humanred.rxns(biomidxred);
        biomlabelsred=humanred.rxnNames(biomidxred);

    end
end



