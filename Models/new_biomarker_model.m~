%% 
clear
close all

%% Load model 
load('recon2v04_mod.mat')

humanGSM=modelR204;
%% add the relevant reactions 
humanGSM.S((abs(humanGSM.S(:,findRxnIDs(humanGSM,'PHYTt')))>0),findRxnIDs(humanGSM,'PHYTt'))
humanGSM.mets((abs(humanGSM.S(:,findRxnIDs(humanGSM,'PHYTt')))>0))

% guanidinoacetic acid: missing exchange reaction, trandport reaction to
% extracellular space 

humanGSM = addReaction(humanGSM,'GUDACt',{'gudac[e]','gudac[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_GUDAC[e]',{'gudac[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
% arginosuccinate: missing exchange reaction, trandport reaction to
% extracellular space 

humanGSM = addReaction(humanGSM,'ARGSUCt',{'argsuc[e]','argsuc[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_argsuc[e]',{'argsuc[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);

% 3-O-methyldopa: missing exchange reaction, transport reaction to
% extracellular space and a consumption reaction
% Creation of two additionnal metabolites
% abbreviation for 3-methoxy-4-hydroxyphenylpyruvate: 3m4hpp /HMDB11714
% abbreviation for 3-methoxy-4-hydroxyphenyllactate: 3m4hpl /HMDB00913
% present in the urine and blood of patients, therefore need for exchange
% and transport reactions

humanGSM = addReaction(humanGSM,'CE2176t',{'CE2176[e]','CE2176[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'CE2176tm',{'CE2176[m]','CE2176[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, mitochondrial','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_CE2176[e]',{'CE2176[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
humanGSM = addReaction(humanGSM,'3M4HPPt',{'3m4hpp[e]','3m4hpp[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'3M4HPPtm',{'3m4hpp[m]','3m4hpp[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, mitochondrial','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_3m4hpp[e]',{'3m4hpp[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
humanGSM = addReaction(humanGSM,'3M4HPLt',{'3m4hpl[e]','3m4hpl[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'3M4HPLtm',{'3m4hpl[m]','3m4hpl[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, mitochondrial','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_3m4hpl[e]',{'3m4hpl[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
humanGSM = addReaction(humanGSM,'CE2176TRA',{'CE2176[m]','akg[m]','3m4hpp[m]',...
    'gln_L[m]'},[-1 -1 1 1],true,-1000,1000,0,'L-dopa degradation','',{},{},true);
humanGSM = addReaction(humanGSM,'3M4HPLFM',{'3m4hpp[m]','nadh[m]','h[m]',...
    'nad[m]','3m4hpl[m]'},[-1 -1 -1 1 1],true,-1000,1000,0,'L-dopa degradation','',{},{},true);

% L-pipecolic acid: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm and cytoplasm to peroxisome
humanGSM = addReaction(humanGSM,'LPIPECOLtx',{'Lpipecol[x]','Lpipecol[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, peroxisome','',{},{},true);
humanGSM = addReaction(humanGSM,'LPIPECOLt',{'Lpipecol[e]','Lpipecol[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_Lpipecol[e]',{'Lpipecol[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);

% galactitol: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'GALTt',{'galt[e]','galt[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_galt[e]',{'galt[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);

% fumarate: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'FUMt',{'fum[e]','fum[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_fum[e]',{'fum[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);

% 17alpha-hydroxypregnenolone: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'17AHPRGNLONEt',{'17ahprgnlone[e]','17ahprgnlone[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_17ahprgnlone[e]',{'17ahprgnlone[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);

% 17alpha-hydroxyprogesterone: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'17AHRPRGSTRNt',{'17ahprgstrn[e]','17ahprgstrn[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_17ahprgstrn[e]',{'17ahprgstrn[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);

% dehydroepiandrosterone : missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'DHEAt',{'dhea[e]','dhea[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_dhea[e]',{'dhea[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);

% pristanic acid : missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'PRISTt',{'prist[e]','prist[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_prist[e]',{'prist[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);

% HC02147 L-oleoylcarnitine : missing exchange reaction and transport reaction from
% extracellular space to cytoplasm, change the reaction reversibility of
% r2436
humanGSM = addReaction(humanGSM,'HC02147t',{'HC02147[e]','HC02147[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_HC02147[e]',{'HC02147[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
humanGSM = addReaction(humanGSM,'HC02147tm',{'HC02147[m]','HC02147[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, mitochondrial','',{},{},true);


% Palmitoylcarnitine: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'PMTCRNte',{'pmtcrn[e]','pmtcrn[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_pmtcrn[e]',{'pmtcrn[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);

% Hexccrn: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'HEXCCRNte',{'hexccrn[e]','hexccrn[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_hexccrn[e]',{'hexccrn[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
% Homocysteine: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'HCYSte',{'hcys_L[e]','hcys_L[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_hcys_L[e]',{'hcys_L[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
% Androst-4-ene-3,17-dione: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'ANDRSTNDNte',{'andrstndn[e]','andrstndn[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_andrstndn[e]',{'andrstndn[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
% mevalonate: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'MEVRt',{'mev_R[e]','mev_R[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_mev_R[e]',{'mev_R[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
% mevalonate: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm 
humanGSM = addReaction(humanGSM,'MEVRt',{'mev_R[e]','mev_R[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_mev_R[e]',{'mev_R[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
% 8-dehydrocholesterol: missing exchange reaction and transport reaction from
% extracellular space to cytoplasm and transformation of 8-DHC to 7-DHC
% (paper Batta et al, ABnormal cholesterol synthesis in SLOS)
humanGSM = addReaction(humanGSM,'CE1297t',{'CE1297[e]','CE1297[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_CE1297[e]',{'CE1297[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
humanGSM = addReaction(humanGSM,'CE1297tr',{'CE1297[r]','CE1297[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, reticulum','',{},{},true);
humanGSM = addReaction(humanGSM,'CE1297tm',{'CE1297[m]','CE1297[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, mitochondrial','',{},{},true);
humanGSM = addReaction(humanGSM,'7DHCHSTEROLt',{'7dhchsterol[e]','7dhchsterol[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, extracellular','',{},{},true);
humanGSM = addReaction(humanGSM,'EX_7dhchsterol[e]',{'7dhchsterol[e]'},-1,true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
humanGSM = addReaction(humanGSM,'7DHCHSTEROLtr',{'7dhchsterol[r]','7dhchsterol[c]'},[-1 1],true,...
    -1000,1000,0,'Transport, reticulum','',{},{},true);
humanGSM = addReaction(humanGSM,'8DHCFM',{'7dhchsterol[r]','CE1297[r]'},[-1 1],true,...
    -1000,1000,0,'Exchange/demand reaction','',{},{},true);
% remove r381 --> duplicate Rxn
humanGSM=removeRxns(humanGSM,'r1381');


%% Reduce the model
%Find exchange reactions 
model=humanGSM;
 [selExc,selUpt] = findExcRxns(model);
   nRxn=length(model.rxns);

 for i=1:nRxn
     if (model.lb(i)==-999999)
         model=changeRxnBounds(model,model.rxns(i),-1000,'l');
     end
      if (model.ub(i)==999999)
         model=changeRxnBounds(model,model.rxns(i),1000,'u');
     end
 end
 %
 model2=changeRxnBounds(model,model.rxns(find(selExc)),-1000,'l');
  model2=changeRxnBounds(model2,model.rxns(find(selExc)),1000,'u');
[modelRed,hasFlux,maxes,mins]=reduceModel_cr(model2,1*10^(-12),false,false,true,true,false)
 
% Find uptake reactions of the former model

UptRxn=model.rxns(find(selUpt));
values_lb=model.lb(find(selUpt));
ids_upt_inModel=findRxnIDs(modelRed,UptRxn);
UptRxn_filtered=modelRed.rxns(nonzeros(ids_upt_inModel));
values_lb_filtered=values_lb(find(ids_upt_inModel));

%
 [selExcWT,selUptWT] = findExcRxns(modelRed);
 modelRed=changeRxnBounds(modelRed,modelRed.rxns(find(selExcWT)),0,'l');
 
 modelRed=changeRxnBounds(modelRed,UptRxn_filtered,values_lb_filtered,'l');
 values_check=modelRed.lb(find(selUptWT));
 %%
 AddedRxns={'GUDACt','EX_GUDAC[e]','ARGSUCt','EX_argsuc[e]','CE2176t',...
     'EX_CE2176[e]','3M4HPPt','3M4HPPtm','EX_3m4hpp[e]','3M4HPLt',...
     '3M4HPLt','EX_3m4hpl[e]','CE2176TRA','3M4HPLFM','LPIPECOLtx',...
     'LPIPECOLt','EX_Lpipecol[e]','GALTt','EX_galt[e]','FUMt','EX_fum[e]',...
     '17AHPRGNLONEt','EX_17ahprgnlone[e]','17AHRPRGSTRNt','EX_17ahprgstrn[e]',...
     'DHEAt','EX_dhea[e]','PRISTt','EX_prist[e]','HC02147t','EX_HC02147[e]',...
     'PMTCRNte','EX_pmtcrn[e]','HEXCCRNte','EX_hexccrn[e]','CE1297t',...
     'EX_CE1297[e]','CE1297tr','CE1297tm','7DHCHSTEROLt','EX_7dhchsterol[e]',...
     '7DHCHSTEROLtr','8DHCFM'};
 ids_addedRxns=findRxnIDs(modelRed, AddedRxns);
RxnToModify=AddedRxns(ids_addedRxns==0);
 %%
 save('reconv04_mod_red_170315.mat','modelRed');