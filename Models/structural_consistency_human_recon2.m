%% STructural consistency human

clear
close all

load('recon2v04_mod.mat')
model=modelR204;

%% Find exchange reactions 
 [selExc,selUpt] = findExcRxns(model);
   nRxn=length(model.rxns);

 for i=1:nRxn
     if (model.lb(i)==-999999)
         model=changeRxnBounds(model,model.rxns(i),-1000,'l');
     end
      if (model.ub(i)==999999)
         model=changeRxnBounds(model,model.rxns(i),1000,'u');
     end
 end
 %%
 yeast_int2=changeRxnBounds(model,model.rxns(find(selExc)),-1000,'l');
  yeast_int2=changeRxnBounds(yeast_int2,model.rxns(find(selExc)),1000,'u');
[modelRed,hasFlux,maxes,mins]=reduceModel(yeast_int2,1*10^(-12),false,false,true,true,false)
 
%% Find uptake reactions of the former model

UptRxn=model.rxns(find(selUpt));
values_lb=model.lb(find(selUpt));
ids_upt_inModel=findRxnIDs(modelRed,UptRxn);
UptRxn_filtered=modelRed.rxns(nonzeros(ids_upt_inModel));
values_lb_filtered=values_lb(find(ids_upt_inModel));

%%
 [selExcWT,selUptWT] = findExcRxns(modelRed);
 modelRed=changeRxnBounds(modelRed,modelRed.rxns(find(selExcWT)),0,'l');
 
 modelRed=changeRxnBounds(modelRed,UptRxn_filtered,values_lb_filtered,'l');
 values_check=modelRed.lb(find(selUptWT));
 
 %% 
%  recon2v04_red=modelRed;
%  save('recon2v04_mod_red.mat','recon2v04_red')