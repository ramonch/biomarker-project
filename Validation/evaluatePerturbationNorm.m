function perturbnorm=evaluatePerturbationNorm(model,sens,genes,listBiomarkWithCons,option,optionNorm)
% 3 options to determine the magnitude of a perturbation
%   case normalizeOnEntireEffect :
%   case normalizeOnBiomarkerEffect: we calculate the resulting norm of all the effects on the
%   biomarkers
%   case normalizeOnPerturbationMagnitude: We determine the norm of the sensitivities for the collection
%   of perturbed reactions, for example if two reactions in a linear
%   pathway are perturbed we consider only one of the two reactions

% Author: Charlotte Ramon
    ng=size(genes,1);
    perturbnorm=nan(ng,1);
switch option 
    case 'normalizeOnEntireEffect'
        for i=1:ng
            perturbnorm(i)=norm(sens(:,i));
        end
    case 'normalizeOnBiomarkerEffect'
        for i=1:ng
            perturbnorm(i)=norm(listBiomarkWithCons(i,:));
        end
    case 'normalizeOnPerturbationMagnitude'
        N=getNullSpace(model.S);
        for ig=1:ng
            %ig
            [rxnTot, constrainRxn,~] = detectAffectedRxnsByGene(model,genes.Id(ig));
            rxn=rxnTot(constrainRxn);
            if ~ isempty(rxn)
                perturbationApplied=sens(rxn,ig);
                eqnull=N(rxn,:)./repmat(perturbationApplied,1,size(N,2));
                [~,iu]=uniquetol(full(eqnull),'ByRows',true);
                test=full(N(rxn,:));
                nonzero=all(test==0);
                %test(:,~nonzero)
                [~,iu2]=uniquetol(full(N(rxn,:)),'ByRows',true);

                perturbnorm(ig)=norm(sens(rxn(iu),ig));
                %iu
                %if length(iu) ~= length(rxn)
                if length(rxn)>1
                    fprintf('Works for gene %d \n',ig)
                    out=test(:,~nonzero);
                    printRxnFormula(model,model.rxns(rxn))

                end
            end
        end
    case 'normalizeOnPerturbationMagnitudeSimple'
        for ig=1:ng
            %ig
            [rxnTot, constrainRxn,~] = detectAffectedRxnsByGene(model,genes.Id(ig));
            rxn=rxnTot(constrainRxn);
            if ~ isempty(rxn)
                
                if ~strcmp(version,'constraintsOneRxn')
                    perturbationApplied=sens(rxn(1),ig);
                    perturbnorm(ig)=norm(perturbationApplied,ig);
                else
                    perturbationApplied=sens(rxn,ig);
                    perturbnorm(ig)=norm(perturbationApplied,ig);
                end

            end
        end
end