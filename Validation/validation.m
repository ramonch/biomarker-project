
clear 
close all hidden

%% Load models and add cobra toolbox
p=genpath('/Users/ramonch/Documents/MATLAB/cobratoolbox/');
addpath(p)
initCobraToolbox();


p=genpath('/Users/ramonch/Documents/MATLAB/colorBrewer/');
addpath(p)

p=genpath('/Users/ramonch/Documents/PhD_project/Predicting biomarkers Ruppin versus sensitivity/');
addpath(p)
%% parameters

testcase=13;
folderGene='../Data publications/';
folderModel='../Models/';
flDisplayFigure=false;
tolerance =10^(-12);
flreduceModel=true;
fluseAllExch=true;
flretrieveGenes=false;
maxFVA=0;
flFVA=0;
flpert=1;
flAnalyzeCompendium=3;

%%
filetestcase=['../initialization files/testcase' num2str(testcase) '_init.mat'];
if exist(filetestcase,'file')
    load(filetestcase)
else
    initializeBiomarkerPrediction;
    if ~exist(filetestcase,'file')
        save(filetestcase)
    end
end

%%
switch flAnalyzeCompendium
    case 1
        trueBiom=analyzeCompendiumFiles();
        trueBiom.source=repmat({'Compendium'},size(trueBiom,1),1);
        trueBiom.type=repmat({'all'},size(trueBiom,1),1);
        datacase='compendium';
    case 2
        publifile='publication_recon2_IEM_data.xlsm';
        trueBiom=analyze_real_biomarkers_file(folderGene,publifile);
        datacase='57genes';
        
    case 3
        publifile='publication_recon2_Zamboni.xlsm';
        trueBiom=analyze_real_biomarkers_file(folderGene,publifile);
        datacase='zamboni';

end
%%
genes=table;
[genes.Id,za,zc]=unique(trueBiom.id,'stable');
genes.disease=trueBiom.disease(za);
genes.idm=findGeneIDs(humanred,genes.Id);
for ig=1:size(genes,1)                                                                        
    g=genes.idm(ig);
    [rxnTot, constrainRxn,~] = detectAffectedRxnsByGene(humanred,genes.Id(ig));
    genes.nbrxn(ig)=sum(constrainRxn);
    genes.rxns{ig}=humanred.rxns(rxnTot(constrainRxn));
end
%
ng=size(genes,1);
nbiom=length(biomidx);
percentages=[0.001, 0.01, 0.05];
exbool=startsWith(humanred.rxns,'EX_');


genesWithNoRxn=genes.nbrxn==0;
geneidWithNoRxn=genes.Id(genesWithNoRxn);
ngenesWithNoRxn=sum(genesWithNoRxn);
fprintf('There are %d genes with no reactions associated \n',ngenesWithNoRxn);

genes=genes(~genesWithNoRxn,:);
fprintf('There are %d genes left out of %d \n',size(genes,1),ng);

lia=ismember(trueBiom.id,geneidWithNoRxn);
trueBiom=trueBiom(~lia,:);

%% Change reactions rules for some genes

% example Crigler-Najjar syndrome type II, it has been shown that UGT1 is
% the physiological important bilirubin glucuronidating forms and thus the
% rule should be changed

ugtrxn=findRxnIDs(humanred,'UGT1A2r');
humanred.rules{ugtrxn}='x(1945)';

% arginase deficiency : 
argrxn=findRxnIDs(humanred,'ARGN');
humanred.rules{ugtrxn}='x(334)';

%%

[listbiomark, madbiomark]=predict_biomarkers_sens_genes(humanred,genes, SMred,...
    biomidxred,biomlabelsred,flDisplayFigure,tolerance,2.5,exbool);
%%
[listbiomarkRup,~]=predict_biomarkers_ruppin_genes(humanred,genes,...
    biomred,percentages,maxFVA,flFVA,flpert);
%%  Computing the center of the polytope
centerfilename=['testcase' num2str(testcase) '_center' '.mat'];
if ~exist(centerfilename,'file')
    % need to check the redundant constraints 
    humanred.lb(humanred.c==1)=1;
    center=findMVECenter(humanred);
    nRxns=length(humanred.rxns);

    %check that the center fulfils the steady-state equation
    norm(humanred.S*center)
    % check that the centers fulfills the inequalities 
    checkub=sum(center<=humanred.ub)==nRxns;
    checklb=sum(center>=humanred.lb)==nRxns;

    % check the value through the growth
    center(humanred.c==1)
    center(startsWith(humanred.rxns,'DM_atp'))

    % chekc the value of the center through the exchange reactions:
    figure
    histogram(center(biomidxred))
    
    save(centerfilename,'center')
else
    load(centerfilename)
end



%%
ig=1:size(genes,1);
%version='softconstraints';
%version='constraints';
version='constraintsOneRxn';
[listBiomarkWithCons,sens]=predictBiomarkersSensWithConstraintsGenes(humanred,genes(ig,:), ...
   center,biomidxred,biomlabelsred,version);


%optionNorm='normalizeOnPerturbationMagnitude';
%optionNorm='normalizeOnBiomarkerEffect';
%optionNorm='normalizeOnEntireEffect';
optionNorm='normalizeOnPerturbationMagnitudeSimple';
perturbnorm=evaluatePerturbationNorm(humanred,sens,genes,listBiomarkWithCons,optionNorm);
perturbnorm(isnan(perturbnorm))=0;
listBiomarkWithConsNorm=listBiomarkWithCons./repmat(perturbnorm,1,size(listBiomarkWithCons,2));

%listBiomarkWithConsNorm=listBiomarkWithConsNorm./repmat(center(biomidxred)',length(ig),1);

%% debugging 

ig=1;
[rxnTot, constrainRxn,~] = detectAffectedRxnsByGene(humanred,genes.Id(ig));
rxn=rxnTot(constrainRxn);
printRxnFormula(humanred,humanred.rxns(rxn))
[lbcons,sgcons]=predictBiomarkersSensWithConstraintsGenes(humanred,genes(ig,:), ...
   center,biomidxred,biomlabelsred,version);
[lb, md,sg]=predict_biomarkers_sens_genes(humanred,genes(ig,:), SMred,...
    biomidxred,biomlabelsred,flDisplayFigure,tolerance,2.5,exbool);


figure
histogram(sg(:,283),100)

listAA={'ile','leu_L','val_L','Lcystin','leuktrE4','34dhphe', '5htrp', 'adrnl', 'dopa(e)','lys_L','orn',...
    'akg','cit(e)','bilirub','gal(e)','glc(e)','tetdec2crn','acac','bhb','acetone','ivcrn','3bcrn'...
    'c5dc'};
idxAA=zeros(length(listAA),1);
for i=1:length(listAA)
    idxAA(i)=find(startsWith(humanred.rxns,['EX_', listAA{i}]));
end

[~,idxAAinBiom]=ismember(idxAA,biomidxred);
[biomark,res]=predictBiomarkersSensWithConstraintsGenes(humanred,genes(ig,:), ...
   center,biomidxred,biomlabelsred,version);
biomark2=biomark/norm(biomark);
tableGene=table;
tableGene.biomark=listAA';
tableGene.biomarkConstraints=listBiomarkWithConsNorm(ig,idxAAinBiom)';
tableGene.biomarkSens=listbiomark(ig,idxAAinBiom)';
tableGene.biomarkRup=listbiomarkRup(ig,idxAAinBiom,3)';
tableGene

[sortedBiom,isortedBiom]=sort(abs(biomark2),'descend');
[humanred.rxns(biomidxred(isortedBiom(1:30))),num2cell(biomark2(isortedBiom(1:30)))']



figure
hist(listBiomarkWithConsNorm(:,find(startsWith(biomred,'EX_h(e)'))),100) %leu

% predictBiomarkersSensWithConstraintsGenes(humanred,genes(50,:), ...
%    center,biomidxred,biomlabelsred,version);
% figure 
% hist(sens.x(biomidxred),100)
% 
% figure(2); clf;
% hist(listBiomarkWithConsNorm(ig,:),100)


%% load results if necessary
fileresults=['results/results_validation_testcase' num2str(testcase) '.mat'];
if exist(fileresults,'file')
    load(fileresults)
end
%% Analyse biomarker prediction

unBiom=table;
[unBiom.biom,~,zc]=unique(trueBiom.biom);

isexrxn=sum(startsWith(unBiom.biom,'EX_'))>0;

if ~isexrxn
[measbiom,measbiomidx,measbiomfound]=findExcRxnFromMets(humanred,unBiom.biom);
else
    measbiomidx=findRxnIDs(humanred,unBiom.biom);
    measbiomfound=measbiomidx~=0;
    measbiom=unBiom.biom(measbiomfound);

end
% this describes whether a particular reaction was found in the model
unBiom.ex(measbiomfound,1)=measbiom';

% measTrueBiom is without the one that can never be found 
trueBiom.ex=unBiom.ex(zc);
measTrueBiom=trueBiom(measbiomfound(zc),:);


[unmeastruebiom,~,zc]=unique(measTrueBiom.biom,'stable');

for z=1:length(unmeastruebiom)
    zt=find(zc==z);
    cat=unique(measTrueBiom.type(zt));
    unknown=strcmp(cat,'');
    if length(cat)==1 
        untype=cat{1};
    else
        cat=cat(~unknown);
        if length(cat)==1
            untype=cat{1};
        else
            untype='both';
        end
    end
    measTrueBiom.untype(zt,1)=repmat({untype},length(zt),1);
end
%%
% filters list biomark based on true biomarkers
exportFig=false;
toplot='MCC';
statperbiom=false;
accountDirectionChange=false;
if statperbiom
    whatisplotted='per Biomarkers';
else
    whatisplotted='per Disease';
end


if accountDirectionChange
    accountDirectionstr='accountDirection';
else
    accountDirectionstr='';
end


fprintf('Plotting %s %s \nVersion of the algorithm used: %s \nNormalization applied %s \n',...
    toplot,whatisplotted,version,optionNorm)
%
%mad=3:0.25:3.5;
mad=31:1:33;

%threshPerc=0.10:0.025:0.15;
threshPerc=0.15:0.05:0.25;
threshPerc=0.05:0.01:0.07
%threshPerc=0.03:0.01:0.05
%threshPerc=0.01:0.01:0.03;
%threshPerc=0.09:0.01:0.11
%threshPerc=0.11:0.01:0.13
%threshPerc=0.25:0.1:0.45;

%threshPerc=20:2:24; 

cmap1=brewermap(3,'Blues');
cmap2=brewermap(3,'Reds');
ng=height(genes);
source=unique(measTrueBiom.source);
biomtype=unique(measTrueBiom.type);
sourceall=[{'All'};source];
biomtypeall=[{'All'};biomtype];
warning off
for zs=0%:length(source)
    for zbt=0%:length(biomtype)
        cnt=1;
        stat=table;
        sel=ones(height(measTrueBiom),1);
        if zs>0
            sel=sel.*strcmp(measTrueBiom.source,source{zs});
        end
        if zbt>0
            sel=sel.*strcmp(measTrueBiom.type,biomtype{zbt});
        end
        zmeasTrueBiom=measTrueBiom(logical(sel),:); % filtered true biomarker table

        [measunBiom,za,zc]=unique(zmeasTrueBiom.biom); % unique biomarkers left
        measunbiomex=zmeasTrueBiom.ex(za); % unique biomarkers (echange reaction)
        measuntype=zmeasTrueBiom.untype(za);
        nmeasbiom=length(measunBiom); % number of exchange biomarkers
        [zlia,zlocb]=ismember(measunbiomex,biomred); 
        measlistbiomark=listbiomark(:,zlocb(zlia));
        measmadbiomark=madbiomark(:,zlocb(zlia));
        measlistBiomarkRup=listbiomarkRup(:,zlocb(zlia),:);
        measlistbiomarkWithConstraints=listBiomarkWithConsNorm(:,zlocb(zlia));

        validationMatrix=zeros(ng,nmeasbiom);
        for zi=1:height(zmeasTrueBiom)
            [~,zx]=ismember(zmeasTrueBiom.id(zi),genes.Id);
            zy=zc(zi);
            validationMatrix(zx,zy)=zmeasTrueBiom.change(zi);

        end
        classification=zeros([size(validationMatrix),2]);

        for type=1:3
            for parameter=1:3
                switch type
                    case 1
                        predicted=double(measmadbiomark>mad(parameter));
                        stype='sens';
                        sparameter=['mad ',num2str(mad(parameter))];
                    case 2
                        predicted=measlistBiomarkRup(:,:,parameter);
                        stype='Ruppin';
                        sparameter=['perc ',num2str(percentages(parameter))];
                    case 3 
                        predicted=abs(measlistbiomarkWithConstraints)>threshPerc(parameter);
                        predicted=sign(measlistbiomarkWithConstraints).*predicted;
                        stype='sensWithCons';
                        sparameter=['perc ',num2str(threshPerc(parameter))];

                end
                if statperbiom
                    n=nmeasbiom;
                    predicted2=predicted';
                    validationMatrix2=validationMatrix';
                    valtype='perbiomarker';
                else
                    n=ng;
                    predicted2=predicted;
                    validationMatrix2=validationMatrix;
                    valtype='perdisease';

                end
                
                if ~accountDirectionChange
                    validationMatrix2=abs(validationMatrix2);
                    predicted2=abs(predicted2);
                end

                    btps=((predicted2==validationMatrix2).*(predicted2~=0))==1;
                    btns=((predicted2==validationMatrix2).*(predicted2==0))==1;
                    bfps=((predicted2~=validationMatrix2).*(validationMatrix2==0))==1;
                    bfns=((predicted2~=validationMatrix2).*(validationMatrix2~=0))==1;

                    for z=1:n
                        if ~statperbiom && genes.nbrxn(z)~=0
                            stat.type(cnt,1)={[stype,'-',sparameter]};
                            stat.disease(cnt,1)=z;
                            classification(z,btps(z,:),type)=2;
                            classification(z,btns(z,:),type)=0;
                            classification(z,bfns(z,:),type)=-2;
                            classification(z,bfps(z,:),type)=-1;
                            stat.tp(cnt,1)=sum(btps(z,:));
                            stat.tn(cnt,1)=sum(btns(z,:));
                            stat.fp(cnt,1)=sum(bfps(z,:));
                            stat.fn(cnt,1)=sum(bfns(z,:));
                            cnt=cnt+1;
                        end
                    end           
            end
        end

        stat.precision=(stat.tp./(stat.tp+stat.fp));
        stat.recall=(stat.tp./(stat.tp+stat.fn));
        stat.accuracy=(stat.tp+stat.tn)./(stat.tp+stat.tn+stat.fp+stat.fn);
        stat.MCC=(stat.tp.*stat.tn-stat.fp.*stat.fn)./...
            sqrt((stat.tp+stat.fp).*(stat.tp+stat.fn).*(stat.tn+stat.fp).*(stat.tn+stat.fn));
        stat.MCC(isnan(stat.MCC))=0;
        stat.precision(isnan(stat.precision))=0;

        foo=boxplotwithdata(stat,toplot,'type');
        ylabel([toplot, ' (-)'])
        title (['Source:',sourceall{zs+1},' Type:', biomtypeall{zbt+1}])
        formatFigureBiomarker(foo,0.6);
        if exportFig
            fn=['results/testcase', num2str(testcase), '_',toplot,'_',version,'_',valtype,...
                '_',datacase,'_',optionNorm,'_',accountDirectionstr];
            fprintpdffig(foo,[fn,'_AllRxn'],'pdf')
        end
    end
end



dis=find(genes.nbrxn==1);
hasOneRxn=ismember(stat.disease,dis);
statred=stat(hasOneRxn,:);
foo=boxplotwithdata(statred,toplot,'type');
ylabel([toplot, ' (-)'])
formatFigureBiomarker(foo,0.6)
if exportFig
    fn=['results/testcase', num2str(testcase), '_',toplot,'_',version,'_',valtype,...
        '_',datacase,'_',optionNorm,'_',accountDirectionstr];
    fprintpdffig(foo,[fn,'_OneRxn'],'pdf')
end

statSensCons=stat(strcmp(stat.type,'sensWithCons-perc 0.1'),:);
dislowMCC=statSensCons.disease(statSensCons.MCC<0.05);

% gscatterfacealpha(stat.recall,stat.precision,stat.type,[cmap1(3,:);cmap2(3,:)],0.5);
% xlabel('Recall')
% ylabel('Precision')





% figure
% method=unique(stat.('type'));
% untype=unique(measuntype);
% measuntype2=[measuntype;measuntype];
% nm = length(method);
% d=cell(1,1);
% cnt=1;
% labels=cell(1,1);
% for zut=1:length(untype)
%     for z = 1:nm
%         if ~iscell(stat)
%             d{cnt} = stat.(toplot)((strcmp(stat.('type'),method(z)).*strcmp(measuntype2,untype(zut)))==1);
%             labels{cnt}=[untype{zut},'-',method{z}];
%             cnt=cnt+1;
%         end
%     end
% end
% [xt,hd,hm] = dotplot(d);
% hold on;
% n=length(labels);
% plot([.5 n+.5],[0 0],'k--');
% set(gca,'XTick',1:n,'XTickLabel',labels,'XTickLabelRotation',45);
% 
% figure
% boxplot(stat.accuracy,stat.type)
% ylabel('Accuracy (-)')
% 
% figure
% boxplot(stat.mcc,stat.type)
% ylabel('MCC (-)')
%% Find out the biomarkers with poor statistics 

stattypes=unique(stat.type);
statfiltered=stat(strcmp(stat.type,stattypes(end)),:);
biomfound=measunbiomex(setdiff(1:size(statfiltered,1),find(statfiltered.MCC<=0)));
zzid=find(statfiltered.MCC<=0);
biomneverfound=measunbiomex(statfiltered.MCC<=0);
[~,biomneverfoundid]=ismember(biomneverfound,humanred.rxns);
[~,biomfoundid]=ismember(biomfound,humanred.rxns);

[biomneverfound, num2cell(center(biomneverfoundid))]
[biomfound, num2cell(center(biomfoundid))]

figure
hist(measlistbiomarkWithConstraints(:,zzid(8)),100)
genes.Id(find(abs(measlistbiomarkWithConstraints(:,4))>0.05))
%% Produce heatmap
ig=1:height(genes);
figure
h=heatmap(validationMatrix(ig,:,1));
h.Title = 'validation matrix';
h.XLabel = 'Biomarkers';
h.YLabel = 'Genes';
h.Colormap=brewermap(11,'RdBu');
h.XDisplayLabels=measunBiom;
h.YDisplayLabels=genes.Id(ig);
%% Heat map sensitivity with constraints

ig=1:height(genes);
figure
h=heatmap(measlistbiomarkWithConstraints(ig,:,1));
h.Title = 'Predictions of biomarkers with constraints';
h.XLabel = 'Biomarkers';
h.YLabel = 'Genes';
h.Colormap=brewermap(11,'RdBu');
h.XDisplayLabels=measunBiom;
h.YDisplayLabels=genes.Id(ig);

%% Heat map sensitivity 

ig=1:height(genes);
figure
h=heatmap(double(measmadbiomark(ig,:,1)>30));
h.Title = 'Predictions of biomarkers with constraints';
h.XLabel = 'Biomarkers';
h.YLabel = 'Genes';
h.Colormap=brewermap(11,'RdBu');
h.XDisplayLabels=measunBiom;
h.YDisplayLabels=genes.Id(ig);

% figure
% HM=HeatMap(flipud(classification(ig,:,1)), 'ColumnLabels',measunBiom,...
%     'ColorMap','redbluecmap','RowLabels',flipud(genes.disease(ig)),'Annotate',true);
% CG1=clustergram(flipud(classification(ig,:,1)), 'ColumnLabels',measunBiom,...
%     'ColorMap','redbluecmap','RowLabels',flipud(genes.disease(ig)),'Annotate',true);
% plot(CG1)
% set(gcf,'Visible','off')
% format_figure_alignment(gcf,1.5)
% print(['results/clustergram_sens_tescase',num2str(testcase)],'-dpdf','-painters')
% 
% CG2=clustergram(flipud(classification(ig,:,2)), 'ColumnLabels',measunBiom,...
%     'ColorMap','redbluecmap','RowLabels',flipud(genes.disease(ig)),'Annotate',true);
% f2=plot(CG2);
% format_figure_alignment(gcf,1.5)
% print(['results/clustergram_ruppin_tescase',num2str(testcase)],'-dpdf','-painters')
% 
% HM=HeatMap(flipud(validationMatrix(ig,:)), 'ColumnLabels',measunBiom,...
%     'ColorMap','redbluecmap','RowLabels',flipud(genes.disease(ig)),'Annotate',true);
% 
% figure
% HM=HeatMap(flipud(measmadbiomark(ig,:)), 'ColumnLabels',measunBiom,...
%     'ColorMap','redbluecmap','RowLabels',flipud(genes.disease(ig)),'Annotate',true,'AnnotColor','k');
% 
% zd=57;
% dis=genes.disease(zd)
% tr=measunBiom(validationMatrix(zd,:)~=0)
% prs=measunBiom(measmadbiomark(zd,:)>mad(3)~=0)
% prr=measunBiom(measlistBiomarkRup(zd,:,3)~=0)
% 
% 
% 
% diffMCC=stat.MCC(1:116)-stat.MCC(117:end);
% figure
% scatter(genes.nbrxn,diffMCC)
% 
% figure
% scatter(stat.MCC(1:116),stat.MCC(117:end))
% figure
% HM=HeatMap(flipud(measmadbiomark(ig,:)), 'ColumnLabels',measunBiom,...
%     'ColorMap','redbluecmap','RowLabels',flipud(genes.Id(ig)),'Annotate',true);
% 
% [listbiomark_test, madbiomark_test]=predict_biomarkers_sens_genes(humanred,genes(ig,:), SMred,...
%     biomidxred,biomlabelsred,flDisplayFigure,tolerance,2.5);
% 
% figure
% HM=HeatMap(flipud(madbiomark_test(1,zlocb(zlia))), 'ColumnLabels',measunBiom,...
%     'ColorMap','redbluecmap','RowLabels',flipud(genes.Id(ig)),'Annotate',true);



%% 

ig=1%1:height(genes);

x=measlistbiomarkWithConstraints(ig,:);
thresh=0.08;
x2(abs(x)<thresh)=0;
x2(x>thresh)=1;
x2(x<-thresh)=-1;


figure
HM=HeatMap(flipud(x2), 'ColumnLabels',measunBiom,...
    'ColorMap','redbluecmap','RowLabels',flipud(genes.disease(ig)),'Annotate',true);
