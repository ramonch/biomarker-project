function trueBiom=analyze_real_biomarkers_file(folderGene,publifile)

filetrueBiom=[folderGene, publifile];
opts=detectImportOptions(filetrueBiom);
opts = setvartype(opts,'EntrezNameId','char');
trueBiomtable=readtable(filetrueBiom,opts, 'Sheet', 'Mapped IEM');

nd=height(trueBiomtable);
trueBiom=table;
source={'Metagene','Compendium', 'Recon2'};
biomtype={'urine','blood',''};
ns=length(source);
nbt=length(biomtype);

cnt=1;
for zd=1:nd    
    for zs=1:ns
        for zt=1:nbt

            disease=trueBiomtable.DiseaseName(zd);
            id=trueBiomtable.EntrezNameId{zd};
            zcol=[biomtype{zt},source{zs}];
            if ismember ([zcol 'Values'],trueBiomtable.Properties.VariableNames)
                zbiom=strsplit(trueBiomtable.([zcol 'Values']){zd},', ');
                zbiomname=strsplit(trueBiomtable.([zcol 'FullNames']){zd},', ');
                zchange=strsplit(trueBiomtable.([zcol 'Change']){zd},', ');
                if ~strcmp(zbiom{1}, '')
                    znb=length(zbiom);
                    trueBiom.disease(cnt:cnt+znb-1,1)=repmat(disease,znb,1);
                    trueBiom.id(cnt:cnt+znb-1,1)=repmat({id},znb,1);
                    trueBiom.biom(cnt:cnt+znb-1,1)=zbiom';
                    trueBiom.biomname(cnt:cnt+znb-1,1)=zbiomname';
                    trueBiom.source(cnt:cnt+znb-1,1)=repmat(source(zs),znb,1);
                    trueBiom.type(cnt:cnt+znb-1,1)=repmat(biomtype(zt),znb,1);
                    zchangenum=zeros(znb,1);
                    zchangenum(strcmp(zchange,'decrease'))=-1;
                    zchangenum(strcmp(zchange,'increase'))=1;
                    trueBiom.change(cnt:cnt+znb-1,1)=zchangenum;
                    cnt=cnt+znb;
                end
            end
        end
    end
end