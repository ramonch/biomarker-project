function new_model=format_met_recon1(model)

new_model=model;
mets=model.mets;

newmets=strrep(mets,'-','_');

new_model.mets=newmets;