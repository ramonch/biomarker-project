function new_model=format_met_ruppinmodel(model)

new_model=model;
mets=model.mets;

newmets=strrep(mets,'_DASH','');
newmets=strrep(newmets,'_COMMA','');

new_model.mets=newmets;