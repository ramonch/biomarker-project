function [list_biomark,delGenes_all]=predict_biomarkers_ruppin_allpert(model,...
rxn,exch_biom,percentages,maxFVA,flFVA)
% model is the genome scale model used 
% rxn are the indices of the (simultaneously) perturbed rxns
% exch_biom are the reaction names of the exchange reactions considered
% maxFVA is the percentage of the objective allowed in the solution
% llFVA is a boolean indicating whether to perform loopless FVA 
p=genpath('/Users/ramonch/Documents/MATLAB/lll-FVA-master/');
addpath(p)

delGenes_all=cell(1,1);
upRegFraction=0.01;
list_biomark=zeros(1,length(exch_biom),length(percentages));
    

list_biomark_int=zeros(1,length(exch_biom),length(rxn),length(percentages));

% Create healthy and disease model
healthy =model;
rxnnames=model.rxns(rxn);
for ir=1:length(rxn)
    ri=rxn(ir);
    ri_n=model.rxns(ri);
    healthy.c=0*healthy.c;
    healthy = changeObjective(healthy,ri_n);
    FBA = optimizeCbModel(healthy,'max');
    lb = optimizeCbModel(healthy,'min');
    [lb.f,FBA.f]
    if FBA.f >0
        healthy = changeRxnBounds(healthy,ri_n,upRegFraction*FBA.f,'l');
    else
        FBA = optimizeCbModel(healthy,'min');
        healthy = changeRxnBounds(healthy,ri_n,upRegFraction*FBA.f,'u');
        healthy = changeRxnBounds(healthy,ri_n,-1000,'l');
    end
end
disease = changeRxnBounds(model,rxnnames,0,'b');

healthy.c=model.c;
% Flux variability analysis healthy
fbasol=optimizeCbModel(healthy);
if fbasol.stat==1
    switch flFVA
        case 0
            %[minFlux_h,maxFlux_h] = fluxVariability_cr(healthy,exch_biom,maxFVA);
            [minFlux_h,maxFlux_h] = fluxVariability(healthy,maxFVA,'max',exch_biom);

        case 1
            [minFlux_h,maxFlux_h] = fluxVariabilityLLC(healthy,maxFVA,'max',exch_biom,0,-2);
    end
else
    minFlux_h=-Inf*ones(length(exch_biom),1);
    maxFlux_h=Inf*ones(length(exch_biom),1);

end
% Flux variability analysis disease
%[minFlux_d,maxFlux_d] = fluxVariability(disease,exch_biom,maxFVA);
[minFlux_d,maxFlux_d] = fluxVariability(disease,maxFVA,'max',exch_biom);

%         if irrev
%             [exch_biom,num2cell(minFlux_h),num2cell(maxFlux_h),num2cell(minFlux_d),...
%                 num2cell(maxFlux_d)]
% %         else
% %             [exch_biom,num2cell(minFlux_hb),num2cell(maxFlux_hb),num2cell(minFlux_h),num2cell(maxFlux_h),num2cell(minFlux_d),...
% %                 num2cell(maxFlux_d)]
%         end
res=table;
res.a1=minFlux_h;
res.a2=maxFlux_h;
res.b1=minFlux_d;
res.b2=maxFlux_d;


for p=1:length(percentages)
    perc=percentages(p);
    pred_biomarker=compare_intervals_biomarkers_ruppin(exch_biom,minFlux_h,...
        maxFlux_h,minFlux_d,maxFlux_d,perc);
    list_biomark_int(1,:,ir,p)=pred_biomarker;
end


for p=1:length(percentages)
    list_biomark(1,:,p)=sum(reshape(list_biomark_int(1,:,:,p),length(exch_biom),length(rxn)),2);
%         i_neg=find(list_biomark{ig}(:,p)<=-1);
%         i_pos=find(list_biomark{ig}(:,p)>=1);
%         vector_i=zeros(1,length(exch_biom));
%         vector_i(i_neg)=-1;
%         vector_i(i_pos)=1;
%         list_biomark(ig,:,p)=squeeze(vector_i);
end


