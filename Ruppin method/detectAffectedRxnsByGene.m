function [rxnInd, constrainRxn,delGenes] = detectAffectedRxnsByGene(model,gene)
% gene ID

if ~isfield(model,'rxnGeneMat')
    %We need the reactionGeneMat during deleteModelGenes, so build it in
    %the beginning
    model = buildRxnGeneMat(model);
end


% detect whether there are alternate transcripts and if they are delete all
% of them 
[gene,rem] = strtok(gene,'.');
allgenes=strtok(model.genes,'.');

delGenes = model.genes(strcmp(gene,allgenes));

if ~isempty(delGenes)
    geneInd=findGeneIDs(model, delGenes);

    %mark genes for deletion
    model.genes(geneInd) = strcat(model.genes(geneInd),'_deleted');
    % find affected reactions 
    x = true(size(model.genes));

    rxnInd = find(any(model.rxnGeneMat(:,geneInd),2));
    x(~cellfun('isempty',(regexp(model.genes,'_deleted')))) = false;

    constrainRxn = false(length(rxnInd),1);
        % Figure out if any of the reaction states is changed
    for j = 1:length(rxnInd)
        if (isfield(model, 'rules') && ~isempty(model.rules{rxnInd(j)})) %To avoid errors if the rule is empty
            if (~eval(model.rules{rxnInd(j)}))
                constrainRxn(j) = true;
            end
        end
    end
else
    rxnInd=0;
    constrainRxn=false;
end

