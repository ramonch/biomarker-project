function [list_biomark,delGenes_all]=predict_biomarkers_ruppin_genes(model,...
genes,exch_biom,percentages,maxFVA,flFVA,flpert)

%flpert is a boolean indicating if we upregulate all affected reactions
%simultaneously or not
% flFVA is a boolean : indicates if we are using classical FVA or
% loplessFVA
p=genpath('/Users/ramonch/Documents/MATLAB/lll-FVA-master/');
addpath(p)

eps=10;
sg=size(genes);
delGenes_all=cell(sg(1),1);

list_biomark=zeros(sg(1),length(exch_biom),length(percentages));
    
for ig=1:sg(1)
    ig
    g=genes.idm(ig);
    [rxnTot, constrainRxn,delGenes] = detectAffectedRxnsByGene(model,genes.Id(ig));
    delGenes_all{ig}=delGenes;
    rxn=rxnTot(constrainRxn);
    reac_name=model.rxns(rxn);
    disp('How many reactions concerned?')
    length(rxn)
    if ~isempty(rxn)
        if flpert
            [list_biomarkg,delGenesg]=predict_biomarkers_ruppin_allpert(model,rxn,exch_biom,percentages,maxFVA,flFVA);
        else
            [list_biomarkg,delGenesg]=predict_biomarkers_ruppin(model,rxn,...
            exch_biom,percentages,maxFVA,flFVA);
        end
        list_biomark(ig,:,:)=list_biomarkg;
        delGenes_all{ig}=delGenesg;
    else
        disp('No associated reaction')
        list_biomark(ig,:,:)=zeros(1,length(exch_biom),length(percentages));
    end
end
