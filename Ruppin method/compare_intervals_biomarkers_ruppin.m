function biomarker=compare_intervals_biomarkers_ruppin(reacExc,minFlux_h,maxFlux_h,minFlux_d,maxFlux_d,perc)
a1=minFlux_h;
a2=maxFlux_h;
b1=minFlux_d;
b2=maxFlux_d;

ex_i=1:length(reacExc);
list=zeros(1,length(ex_i));
% 0 means no change
% 1 means increase in disease state
% -1 means decrease in disease state
for i=1:length(ex_i)
    nb=ex_i(i);
    if nb==0
        list(i)=-100;
    else
        bool1inf=(is10inf(a1(nb),b1(nb),perc)&&a2(nb)<=b2(nb));
        bool2inf=(a1(nb)<=b1(nb)&&is10inf(a2(nb),b2(nb),perc));
        bool1sup=(is10sup(a1(nb),b1(nb),perc)&&a2(nb)>=b2(nb));
        bool2sup=(a1(nb)>=b1(nb)&&is10sup(a2(nb),b2(nb),perc));
        if bool1inf||bool2inf
            list(i)=1;
        elseif bool1sup||bool2sup
            list(i)=-1;
        else
            list(i)=0;
        end
    end
biomarker=list;
end
end
    function bool=is10inf(a,b,perc)
    bool=0;
     if (a>=0 && b>=0)
         bool=(a/(1-perc)<b);
     elseif (a<0 && b>=0)
         bool=1;
     elseif (a<0 && b<0)
         bool=(-a>-b/(1-perc));
     end
     end
     function bool=is10sup(a,b,perc)
     bool=0;
     if (a>=0 && b>=0)
         bool=(a>b/(1-perc));
     elseif (a>=0 && b<0)
         bool=1;
     elseif (a<0 && b<0)
         bool=(-a/(1-perc)<-b);
     end
end