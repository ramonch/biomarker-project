
%%
clear
close all

load ('recon2v04_mod_red_absSens_compendiumPlasma_170316.mat');
%%
for j=1:length(percentages)
HMO=HeatMap(flipud(list_biomark(:,:,j)),'ColumnLabels',Poss_biomarkers,'RowLabels',flipud(gene_map_publi2(:,1)),...
    'Colormap',flipud(redbluecmap(3)))
addTitle(HMO,horzcat('perc=',num2str(percentages(j))))
plot(HMO)
colorbar
grid on
end
%%

%% Get a cell with correct predictions
perc_id=2;
predicted_biomarkers_ruppin=cell(length(geneids),6);
biomarkers_per_disease=cellfun(@(c) strsplit(c,', '),list_biomarker_disease,'uni',0);
TP=3;
TN=4;
FP=5;
FN=6;
for i_gene=1:length(geneids)
    predicted_pos_biomarkers=exc_reac_biom(logical(list_biomark(i_gene,:,perc_id)));
    [true_pos_biomarkers,true_pos_biomarkers_ids] =...
        findExcRxnFromMets(humanGSM,biomarkers_per_disease{i_gene});
    true_neg_biomarkers=setdiff(exc_reac_biom,true_pos_biomarkers);
    predicted_biomarkers_ruppin{i_gene,1}=...
        strjoin(predicted_pos_biomarkers,',');
    predicted_biomarkers_ruppin{i_gene,2}=...
        strjoin(true_pos_biomarkers,',');
    predicted_neg_biomarkers=setdiff(exc_reac_biom,predicted_pos_biomarkers);
    TP=length(intersect(predicted_pos_biomarkers,true_pos_biomarkers));
    FP=length(intersect(predicted_pos_biomarkers,true_neg_biomarkers));
    TN=length(intersect(predicted_neg_biomarkers,true_neg_biomarkers));
    FN=length(intersect(predicted_neg_biomarkers,true_pos_biomarkers));
    predicted_biomarkers_ruppin(i_gene,3:6)=[num2cell(TP),num2cell(TN),...
        num2cell(FP),num2cell(FN)];
end
