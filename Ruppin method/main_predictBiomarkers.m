clear
close all hidden
cd('/Users/ramonch/Documents/PhD_project/Predicting biomarkers Ruppin versus sensitivity/Ruppin method/')

%% Load data
testcase=9;
folderGene='../Data publications/';
folderModel='../Models/';
flDisplayFigure=false;
tolerance =10^(-12);
flFVA=0;
flpert=1;
maxFVA=0;
%%
initializeBiomarkerPrediction;
%%
save(['../initialization files/testcase' num2str(testcase) '_init.mat'])

%%
load(['../initialization files/testcase' num2str(testcase) '_init.mat'])
%%
% affected reactions
percentages=[0.01,0.025,0.05];

ig=17%1:size(genes,1);

[rxnTot, constrainRxn,delGenes] = detectAffectedRxnsByGene(human,genes.Id(ig));

[healthy,hasEffect,constrRxnNames,upregulatedGenes] = upRegulateModelGenes(human,genes.Id(ig),0.05);


[list_biomark,delGenes_all]=predict_biomarkers_ruppin_genes(human,...
genes(ig,:),biom,percentages,maxFVA,flFVA,flpert);
save(['results/testcase' num2str(testcase) '.mat'])

list_biomark(list_biomark>=1)=1;
list_biomark(list_biomark<=-1)=-1;

%%
for p=1:length(percentages)
    figure
    HM=HeatMap(flipud(list_biomark(:,:,p)), 'ColumnLabels',biomark.biom,...
        'ColorMap','redbluecmap','RowLabels',flipud(genes.Genes(ig)),'Annotate',true);
    addTitle(HM,num2str(percentages(p)))
end

%% Compute the TP,TN,FP and FN from the results
% res.biomark=abs(list_biomark);
% Size_genes=size(list_biomark);
% true_biomark=zeros(Size_genes([1,2]));
% for i_gene=1:Size_genes(1)
% list_mets=strsplit(list_biomarker_disease{i_gene},',');
% % Now we need to represent the ROC curve for the sensitivity
% [~,true_pos_biomarkers_ids] =findExcRxnFromMets(humanGSM,list_mets);
% [~,IA,~]=intersect(exc_reac_biom_ids,true_pos_biomarkers_ids);
% true_biomark(i_gene,IA)=1;
% end
% res.true=true_biomark;
% for k=1:Size_genes(3)
%  predicted_biomarkers=res.biomark(:,:,k);
%  res.TP(k)=sum(sum(predicted_biomarkers.*true_biomark));
%  res.FP(k)=sum(sum(predicted_biomarkers.*(true_biomark==0)));
%  res.TN(k)=sum(sum((predicted_biomarkers==0).*(true_biomark==0)));
%  res.FN(k)=sum(sum((predicted_biomarkers==0).*(true_biomark)));
%  res.precision(k)=res.TP(k)./(res.TP(k)+res.FP(k));
%  res.sensitivity(k)=res.TP(k)./(res.TP(k)+res.FN(k));
%  res.specificity(k)=res.TN(k)./(res.TN(k)+res.FP(k));
% end