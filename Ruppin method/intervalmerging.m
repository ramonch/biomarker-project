function [u1vec,u2vec,disj]=intervalmerging(a1vec,a2vec,b1vec,b2vec)
u1vec=zeros(length(a1vec),1);
u2vec=zeros(length(a1vec),1);
disj=zeros(length(a1vec),1);
for i=1:length(a1vec)
    a1=a1vec(i);
    a2=a2vec(i);
    b1=b1vec(i);
    b2=b2vec(i);
    if (a2<b1)||(b2<a1)
        disjoint=1;
        u1=0;
        u2=0;
    else
        disjoint=0;
        if (a1<=b1 && a2>=b1 && a2<=b2)
            u1=a1;
            u2=b2;
        elseif (a1<=b1 && a2>=b1 && a2>=b2)
            u1=a1;
            u2=a2;
        elseif (a1>b1 && a1<=b2 && a2>=b2)
            u1=b1;
            u2=a2;
        elseif (a1>b1 && a1<=b2 && a2<=b2)
            u1=b1;
            u2=b2;
        else
            u1=0;
            u2=0;
            disp('Error: not part of these conditions')
        end
    end
    u1vec(i)=u1;
    u2vec(i)=u2;
    disj(i)=disjoint;
end
end
