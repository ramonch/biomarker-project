function trueBiom=analyzeCompendiumFiles()

    load('IEM_biomarkerList.mat');
    IEMCompendium;
    bm=Swagatika_IEM_biomarker;

    %%
    trueBiom=table;
    count=1;
    for i=1:size(bm,1)
        sameDisease=find(strcmp(IEMs(:,1),bm{i,1}));
        nSameDisease=length(sameDisease);
        vecInBiom=count:count+nSameDisease-1;
        trueBiom.disease(vecInBiom)=repmat(bm(i,1),nSameDisease,1);
        trueBiom.id(vecInBiom)=IEMs(sameDisease,2);
        trueBiom.biom(vecInBiom)=repmat(bm(i,2),nSameDisease,1);
        trueBiom.change(vecInBiom)=repmat(str2double(bm(i,3)),nSameDisease,1); 
        count=count+nSameDisease;
    end
    
    trueBiom.id=strcat(trueBiom.id,'.1');

end