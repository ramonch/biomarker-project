

function [biomark,madBiomark,sens]=predict_biomarkers_sens_genes(model,genes, SM,biomidx,biomlabels,...
    flDisplayFigure,tolerance,thresh,exbool)
% genes structure 
ng=size(genes,1);
nbiom=length(biomidx);
[biomark,madBiomark,sens]=deal(nan(ng,nbiom));
N=full(model.S);
%[U,E,V] = svd(N');

parfor ig=1:ng
    g=genes.idm(ig);
    [rxnTot, constrainRxn,~] = detectAffectedRxnsByGene(model,genes.Id(ig));
    rxn=rxnTot(constrainRxn);
%     [bm,mb]=predict_biomarkers_sens(model,{U,E,V},rxn, SM,biomidx,biomlabels,...
%     flDisplayFigure,tolerance,thresh);
    [bm,mb,sg]=predict_biomarkers_sens(model,N,rxn, SM,biomidx,biomlabels,...
    flDisplayFigure,tolerance,thresh,exbool);
    biomark(ig,:)=bm;
    madBiomark(ig,:)=mb;
    sens(ig,:)=sg;
end
        
    
end