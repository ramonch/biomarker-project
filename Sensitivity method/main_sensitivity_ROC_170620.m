%% Get the biomarkers from the structural sensitivity values
clear
close all

%cd ('/Users/ramonch/Documents/PhD project/HumanGSMmodel/BM_Sens')
%addpath(genpath(pwd));
%% load model
load('recon2v04_mod_red.mat');
humanGSM=recon2v04_red; 
load('publication2.mat')

% load('recon2v04_mod_red_170315.mat');
% humanGSM=modelRed; 
% load('compendium_publi_plasma_170315.mat')

% load('recon2v04_mod_red_170316.mat');
% humanGSM=modelRed; 
% load('compendium_publi_all_170316.mat')

[exc_reac_biom,exc_reac_biom_ids]=findExcRxnFromMets(humanGSM,Poss_biomarkers);

%% Check number of reactions and remove corresponding reactions and expand cells of real biomarkers
biomarkers_per_disease=cellfun(@(c) strsplit(c,', '),list_biomarker_disease,'uni',0);
gene_map_publi2(:,2)=cellfun(@(c) strtrim(c),gene_map_publi2(:,2),'uni',0);
geneids=findGeneIDs_cr(humanGSM,gene_map_publi2(:,2));
nb_reactions=zeros(length(geneids),1);
has_rev=zeros(length(geneids),1);
for i =1:length(geneids)
    nb_reactions(i)=length(nonzeros(humanGSM.rxnGeneMat(:,geneids(i))));
    has_rev(i)=sum(humanGSM.rev(humanGSM.rxnGeneMat(:,geneids(i))>0))>0;
end
%% Add a column which is a boolean which determines if the gene is a housekeeping gene
% HK=readtable('HK_genes.xlsx','Range','A:C');
% entrezids2=strsplit(sprintf('%0.1f\n',HK.entrezids+0.1));
% HK.entrezids2=entrezids2(1:end-1)';
% bool_HK=ismember(gene_map_publi2(:,2),HK.entrezids2);

%% Compute absolute sensitivities 
% gene considered 
Size_genes=size(gene_map_publi2);
gene_map_publi2=strtrim(gene_map_publi2);
gene_ids=findGeneIDs(humanGSM,gene_map_publi2(:,2));
predicted_biomarkers=cell(Size_genes(1),6);
sens_poss_biomarkers=zeros(Size_genes(1), length(exc_reac_biom_ids));
sensitivities_all=zeros(Size_genes(1), length(humanGSM.rxns));
directions =cell(Size_genes(1),1);
toleranceHuman=1e-12;
parfor i_gene=1:Size_genes(1)
    i_gene
    [reac_disease, reac_disease_ids]=getReactionsFromGenesGeneral(humanGSM,...
    gene_ids(i_gene),false);
[Sabs_human,max_res_human,Z_human,dir] = l2_sensitivity_crv2_norm1(full(humanGSM.S),reac_disease_ids,... 
    [],true,toleranceHuman);
%[Sabs_human,max_res_human,Z_human,dir] = l2_sensitivity_crv2_norm1(full(humanGSM.S),reac_disease_ids,... 
%    [],true,toleranceHuman);
sens_poss_biomarkers(i_gene,:)=abs(Sabs_human(exc_reac_biom_ids));
sensitivities_all(i_gene,:)=Sabs_human;
directions{i_gene}=num2cell(dir);
end
%% Get the biomarkers
[res,true_biomark,biomark_unique]=biomark_sens_170620(humanGSM,...
    sens_poss_biomarkers,biomarkers_per_disease,list_biomarker_disease,...
    exc_reac_biom_ids);
%%
orange=[242 126 33]./255;
foo=figure ;
foo.Position([3 4])=0.6*foo.Position([3 4]);
specificity_i=1-res.specificity;
plot(specificity_i,res.sensitivity);
hold on 
s1=scatter(0.02,0.25,'MarkerFaceColor',orange);
lg=legend(s1,'Existing method');
lg.Box='off';
axis([0 1.01 0 1.01])
xlabel('1-Specificity')
xticks(0:0.2:1)
yticks(0:0.2:1)
ylabel('Sensitivity')
set(gca,'Fontsize',18)
set(findobj(gca,'type','line'),'linew',2)
set(gca,'XMinorTick','on','YMinorTick','on')
set(0, 'DefaultAxesFontName', 'Arial');
%% Get predictive accuracies
% biomark unique, colonne  de 3 a 6 , TP TN FP FN
% biomark_unique_t = cell2table(biomark_unique,...
%     'VariableNames',{'pred_biom' 'true_biom' 'TP' 'TN' 'FP' 'FN' });
% values1=cell2mat(biomark_unique(:,3:6));
% values1_sum=sum(values1,1);
% PA1=sum(values1_sum([1,2])/sum(values1_sum));
% PPV1=values1_sum(1)/sum(values1_sum([1,3]));
% NPV1=values1_sum(2)/sum(values1_sum([2,4]));
biomark_unique_t=biomark_unique;
precision=sum(biomark_unique_t.TP)/(sum(biomark_unique_t.TP)+sum(biomark_unique_t.FP));
recall=sum(biomark_unique_t.TP)/(sum(biomark_unique_t.TP)+sum(biomark_unique_t.FN));
specificity=sum(biomark_unique_t.TN)/(sum(biomark_unique_t.TN)+sum(biomark_unique_t.FP));
% Compute hypergeom p-values for each result (disease)
% hygecdf(x,M,K,N) M=size of the population,  K number in the population
% with positive conditions , N number of samples drawn
pValues=1-hygecdf(biomark_unique_t.TP_all-1,54,biomark_unique_t.TP_all+biomark_unique_t.FN_all,...
    biomark_unique_t.TP_all+biomark_unique_t.FP_all);
biomark_unique_t.pValues=pValues;

pValueTotal=1-hygecdf(sum(biomark_unique_t.TP)-1,54*length(biomark_unique_t.FP),sum(biomark_unique_t.TP+biomark_unique_t.FN),...
    sum(biomark_unique_t.TP+biomark_unique_t.FP));
%% distribution of pvalues
%biomark_unique_t.boolHK=categorical(bool_HK);
foo=figure ;
foo.Position([3 4])=0.6*foo.Position([3 4]);
edges1 = logspace(floor(log10(min(pValues))),log10(0.05),9); 
edges2 = logspace(log10(0.05),log10(max(pValues)),5); 
edges=[edges1,edges2(2:end)];
histogram(pValues,edges,'Normalization','probability')

set(gca,'Fontsize',18)
xlabel('P-Values')
set(gca,'XScale','log')
xticks([10^(-3),10^(-2),10^(-1),10^(0)])
ylabel('Frequency')
set(findobj(gca,'type','line'),'linew',2)
set(0, 'DefaultAxesFontName', 'Arial');
xlim([0 1])
% figure
% hist(pValues(biomark_unique_t.boolHK=='true'))
% %% Debug 
% figure
% sens_poss_biomarkers=sens_poss_biomarkers./max(sens_poss_biomarkers,[],2);
% i=2;
% hist(sens_poss_biomarkers(i,:),50)
% xlim([0 1])
% nb_reactions(i)