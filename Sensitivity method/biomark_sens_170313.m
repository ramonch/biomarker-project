function [biomark,biomark_unique,optimal_threshold]=biomark_sens_170313(model,...
    sens_poss_biomarkers,biomarkers_per_disease,list_biomarker_disease,...
    exc_reac_biom,nb_reactions)

%% ROC curve method 1
Size_genes=size(sens_poss_biomarkers);
for i_gene=1:Size_genes(1)

% Now we need to represent the ROC curve for the sensitivity
[true_pos_biomarkers,true_pos_biomarkers_ids] =findExcRxnFromMets(model,biomarkers_per_disease{i_gene});
true_neg_biomarkers=setdiff(exc_reac_biom,true_pos_biomarkers);
%thres_senss=linspace(0.01,1,101);
thres_senss=logspace(-10,0,101);
[TP,FP,FN,TN]=deal(zeros(1,length(thres_senss)));

for index=1:length(thres_senss)

thres_sens=thres_senss(index);
% filter for the interesting exchange reactions 
% Select the exchange reactions that are above the threshold and find the
% predicted biomarkers 
predicted_pos_biomarkers=exc_reac_biom(sens_poss_biomarkers(i_gene,:)>thres_sens);
predicted_neg_biomarkers=setdiff(exc_reac_biom,predicted_pos_biomarkers);
TP(index)=length(intersect(predicted_pos_biomarkers,true_pos_biomarkers));
FP(index)=length(intersect(predicted_pos_biomarkers,true_neg_biomarkers));
TN(index)=length(intersect(predicted_neg_biomarkers,true_neg_biomarkers));
FN(index)=length(intersect(predicted_neg_biomarkers,true_pos_biomarkers));

end
sensitivity=TP./(TP+FN);
specificity=TN./(TN+FP);

% figure 
% specificity_i=1-specificity;
% plot(specificity_i,sensitivity);
% axis([0 1.1 0 1.1])
% xlabel('1-specificity')
% ylabel('sensitivity')

AUC=trapz(fliplr(1-specificity),fliplr(sensitivity));

% distance to the minimal 
d2=(1-specificity).^2+(1-sensitivity).^2;
[dmin, Imin]=min(d2);
thres_sens_optimal=thres_senss(Imin);
predicted_biomarkers_i=exc_reac_biom(sens_poss_biomarkers(i_gene,:)>thres_sens_optimal)
predicted_biomarkers(i_gene,:)=[{strjoin(predicted_biomarkers_i',', ')},...
list_biomarker_disease{i_gene},num2cell(dmin),num2cell(AUC),num2cell(thres_sens_optimal),...
];

%fprintf('progress %3.1f %% \n',i_gene/Size_genes(1)*100)
end

%% Compute absolute sensitivities with same optimal thresholds
% gene considered 
predicted_biomarkers_unique=cell(Size_genes(1),6);
dmins=cell2mat(predicted_biomarkers(:,3));
AUCs=cell2mat(predicted_biomarkers(:,4));
thres_sens_optimal=cell2mat(predicted_biomarkers(:,5));
optimal_threshold=sum((1-dmins).*thres_sens_optimal)/sum((1-dmins));

for i_gene=1:Size_genes(1)
[true_pos_biomarkers,true_pos_biomarkers_ids] =findExcRxnFromMets(model,biomarkers_per_disease{i_gene});
true_neg_biomarkers=setdiff(exc_reac_biom,true_pos_biomarkers);
predicted_pos_biomarkers=exc_reac_biom(sens_poss_biomarkers(i_gene,:)>optimal_threshold);
predicted_neg_biomarkers=setdiff(exc_reac_biom,predicted_pos_biomarkers);
TP=length(intersect(predicted_pos_biomarkers,true_pos_biomarkers));
FP=length(intersect(predicted_pos_biomarkers,true_neg_biomarkers));
TN=length(intersect(predicted_neg_biomarkers,true_neg_biomarkers));
FN=length(intersect(predicted_neg_biomarkers,true_pos_biomarkers));
predicted_biomarkers_unique(i_gene,:)=[{strjoin(predicted_pos_biomarkers',', ')},...
strjoin(true_pos_biomarkers,','),num2cell(TP),num2cell(TN),num2cell(FP),num2cell(FN)];

end
biomark=predicted_biomarkers;
biomark_unique=predicted_biomarkers_unique;