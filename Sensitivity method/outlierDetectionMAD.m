function [TF,v]=outlierDetectionMAD(x,const)

m=mad(x,1);
med=median(x,'omitnan');
v=abs(x-med)/m;
TF=v>const;
end