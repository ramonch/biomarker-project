%% Get the biomarkers from the structural sensitivity values
clear
close all

%cd ('/Users/ramonch/Documents/PhD project/HumanGSMmodel/BM_Sens')
%addpath(genpath(pwd));
%% load model
testcase=8;
folderGene='../Data publications/';
folderModel='../Models/';
flDisplayFigure=false;
tolerance =10^(-12);

%%
initializeBiomarkerPrediction;

%%
SM=create_feature_matrix_rxns(human, 1:length(human.rxns),tolerance);

save(['../initialization files/testcase' num2str(testcase) '_init.mat'])
%%
load(['../initialization files/testcase' num2str(testcase) '_init.mat'])
%% Reduce model and SM Matrix 
leftrxn=find(~isnan(SM(:,1)));
SMred=SM(leftrxn,leftrxn);
humanred=human;
humanred.rxns=human.rxns(leftrxn);
humanred.rules=human.rules(leftrxn);
humanred.S=human.S(:,leftrxn);
humanred.lb=human.lb(leftrxn);
humanred.ub=human.ub(leftrxn);
humanred.c=human.c(leftrxn);


if isfield(human,'rxnGeneMat')
    humanred.rxnGeneMat=human.rxnGeneMat(leftrxn,:);
end

[~, biomidxred]=ismember(biomidx,leftrxn);

%% Predict biomarkers

[list_biomark,madBiomark]=predict_biomarkers_sens_genes(human,genes([2:3],:),SM,biomidx,biomark.abbrev,false,...
    tolerance);

save(['results/testcase' num2str(testcase) '.mat'])
%%

figure
HM=HeatMap(flipud(list_biomark), 'ColumnLabels',biomark.biom,...
    'ColorMap','redbluecmap','RowLabels',flipud(genes.Genes),'Annotate',true);
