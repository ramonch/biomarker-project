function [biomark,madBiomark,sens]=predict_biomarkers_sens(model,N,rxn, SM,biomidx,biomlabels,...
    flDisplayFigure,tolerance,thresh,exbool)

nbiom=length(biomidx);
[biomark,madBiomark]=deal(nan(1,nbiom));
p=genpath('/Users/ramonch/Documents/PhD_project/Predicting biomarkers Ruppin versus sensitivity/Sensitivity code/');
addpath(p)

if isempty(N)
    stoich=full(model.S);
else
    stoich=N;
end

if ~isempty(rxn)
    biom=model.rxns(biomidx);
    [sg,~,~,~,~]=l2_sensitivity_analysis(stoich,rxn,[],true,tolerance);
    
    res=table;
    res.labels=biomlabels;
    sens=sg(biomidx);
    SM=[SM;sg'];
    exbool=[exbool;0];
    for z=1:nbiom
        ex=biomidx(z);
        X=SM(~exbool,ex);
        [TF,v]=outlierDetectionMAD(X,thresh);
        biomark(z)=-sign(sg(ex))*TF(end);
        madBiomark(z)=v(end);
    end
end
if flDisplayFigure
    sizeSubP=ceil(sqrt(nbiom));
    figure
    for z=1:nbiom
        ex=biomidx(z);
        subplot(sizeSubP,sizeSubP,z)
        h=histogram(SM(~exbool,ex),'BinLimits',[-1,1]);
        [TF,v]=outlierDetectionMAD(SM(~exbool,ex),thresh);
        hold on
        xval=SM(end,ex);
        yval=max(h.Values);
        text(xval,0.8*yval,[num2str(TF(end)) '-' num2str(v(end))])
        line([xval xval],[0 yval],'linewidth',1,'Color','r')
        title(biomlabels(z))
    end
%     figure
%     for z=1:nbiom
%         ex=biomidx(z);
%         subplot(sizeSubP,sizeSubP,z)
%         h=histogram(abs(SM(:,ex)),'BinLimits',[0,1]);
%         [TF,v]=outlierDetectionMAD(abs(SM(:,ex)),3);
%         hold on
%         xval=abs(SM(end,ex));
%         yval=max(h.Values);
%         text(xval,0.8*yval,[num2str(TF(end)) '-' num2str(v(end))])
%         line([xval xval],[0 yval],'linewidth',1,'Color','r')
%         title(biomlabels(z))
%     end
%     figure
%     for z=1:nbiom
%         ex=biomidx(z);
%         subplot(sizeSubP,sizeSubP,z)
%         X=log(abs(SM(:,ex)));
%         h=histogram(X);
%         [TF,v]=outlierDetectionMAD(X,3);
%         hold on
%         xval=log(abs(SM(end,ex)));
%         yval=max(h.Values);
%         text(xval,0.8*yval,[num2str(TF(end)) '-' num2str(v(end))])
%         line([xval xval],[0 yval],'linewidth',1,'Color','r')
%         title(biomlabels(z))
%     end
end
