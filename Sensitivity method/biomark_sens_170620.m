function [res,true_biomark,biom]=biomark_sens_170620(model,...
    sens_poss_biomarkers,biomarkers_per_disease,list_biomarker_disease,...
    exc_reac_biom_ids)
%scaling of the sensitivity 
sens_poss_biomarkers=sens_poss_biomarkers./max(sens_poss_biomarkers,[],2)
%% ROC curve method 1
Size_genes=size(sens_poss_biomarkers);
true_biomark=zeros(Size_genes);
for i_gene=1:Size_genes(1)

% Now we need to represent the ROC curve for the sensitivity
[~,true_pos_biomarkers_ids] =findExcRxnFromMets(model,biomarkers_per_disease{i_gene});
[~,IA,~]=intersect(exc_reac_biom_ids,true_pos_biomarkers_ids);
true_biomark(i_gene,IA)=1;
end

 thres_senss=logspace(-10,0,101);
 [res.TP,res.FP,res.FN,res.TN]=deal(zeros(1,length(thres_senss)));

 for index=1:length(thres_senss)
 
thres_sens=thres_senss(index);
 % filter for the interesting exchange reactions 
 % Select the exchange reactions that are above the threshold and find the
 % predicted biomarkers 
 predicted_biomarkers=sens_poss_biomarkers>thres_sens;
 res.TP(index)=sum(sum(predicted_biomarkers.*true_biomark));
 res.FP(index)=sum(sum(predicted_biomarkers.*(true_biomark==0)));
 res.TN(index)=sum(sum((predicted_biomarkers==0).*(true_biomark==0)));
 res.FN(index)=sum(sum((predicted_biomarkers==0).*(true_biomark)));

 end
 res.sensitivity=res.TP./(res.TP+res.FN);
 res.specificity=res.TN./(res.TN+res.FP);

%  figure 
%  specificity_i=1-res.specificity;
%  plot(specificity_i,res.sensitivity);
% axis([0 1.1 0 1.1])
% xlabel('1-specificity')
% ylabel('sensitivity')

res.AUC=trapz(fliplr(1-res.specificity),fliplr(res.sensitivity));

% distance to the minimal 
res.d2=(1-res.specificity).^2+(1-res.sensitivity).^2;
[dmin, Imin]=min(res.d2);
res.thres_sens_optimal=thres_senss(Imin);
biom.pred=sens_poss_biomarkers>res.thres_sens_optimal;
 biom.TP=sum(sum(biom.pred.*true_biomark));
 biom.FP=sum(sum(biom.pred.*(true_biomark==0)));
 biom.TN=sum(sum((biom.pred==0).*(true_biomark==0)));
 biom.FN=sum(sum((biom.pred==0).*(true_biomark)));
 
[biom.TP_all,biom.FP_all,biom.FN_all,biom.TN_all]=deal(zeros(1,length(Size_genes(1)))); 
for i_gene=1:Size_genes(1)
     biom.TP_all(i_gene)=sum(sum(biom.pred(i_gene,:).*true_biomark(i_gene,:)));
     biom.FP_all(i_gene)=sum(sum(biom.pred(i_gene,:).*(true_biomark(i_gene,:)==0)));
     biom.TN_all(i_gene)=sum(sum((biom.pred(i_gene,:)==0).*(true_biomark(i_gene,:)==0)));
     biom.FN_all(i_gene)=sum(sum((biom.pred(i_gene,:)==0).*(true_biomark(i_gene,:))));
 end
% predicted_biomarkers(i_gene,:)=[{strjoin(predicted_biomarkers_i',', ')},...
% list_biomarker_disease{i_gene},num2cell(dmin),num2cell(AUC),num2cell(thres_sens_optimal),...
% ];

% %fprintf('progress %3.1f %% \n',i_gene/Size_genes(1)*100)
% end
% 
% %% Compute absolute sensitivities with same optimal thresholds
% % gene considered 
% predicted_biomarkers_unique=cell(Size_genes(1),6);
% dmins=cell2mat(predicted_biomarkers(:,3));
% AUCs=cell2mat(predicted_biomarkers(:,4));
% thres_sens_optimal=cell2mat(predicted_biomarkers(:,5));
% optimal_threshold=sum((1-dmins).*thres_sens_optimal)/sum((1-dmins));
% 
% for i_gene=1:Size_genes(1)
% [true_pos_biomarkers,true_pos_biomarkers_ids] =findExcRxnFromMets(model,biomarkers_per_disease{i_gene});
% true_neg_biomarkers_ids=setdiff(exc_reac_biom_ids,true_pos_biomarkers);
% predicted_pos_biomarkers_ids=exc_reac_biom_ids(sens_poss_biomarkers(i_gene,:)>optimal_threshold);
% predicted_neg_biomarkers_ids=setdiff(exc_reac_biom_ids,predicted_pos_biomarkers_ids);
% TP=length(intersect(predicted_pos_biomarkers_ids,true_pos_biomarkers));
% FP=length(intersect(predicted_pos_biomarkers_ids,true_neg_biomarkers_ids));
% TN=length(intersect(predicted_neg_biomarkers_ids,true_neg_biomarkers_ids));
% FN=length(intersect(predicted_neg_biomarkers_ids,true_pos_biomarkers));
% predicted_biomarkers_unique(i_gene,:)=[{strjoin(predicted_pos_biomarkers_ids',', ')},...
% strjoin(true_pos_biomarkers,','),num2cell(TP),num2cell(TN),num2cell(FP),num2cell(FN)];
% 
% end
% biomark=predicted_biomarkers;
% biom=predicted_biomarkers_unique;