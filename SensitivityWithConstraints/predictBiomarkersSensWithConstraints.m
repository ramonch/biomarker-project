function [biomark,res]=predictBiomarkersSensWithConstraints(model,rxn, center,biomidx,biomlabels,version)


nbiom=length(biomidx);
biomark=nan(1,nbiom);
res=struct;
%p=genpath('/Users/ramonch/Documents/PhD_project/Predicting biomarkers Ruppin versus sensitivity/Sensitivity code/');
%addpath(p)

if ~isempty(rxn)
    switch version
        case 'constraints'
            sens=sensitivityWithConstraints(model,rxn,center);
        case 'softconstraints'
            sens=sensitivityWithSoftConstraints(model,rxn,center);
        case 'softconstraintsFormer'
            sens=sensitivityWithSoftConstraintsFormer(model,rxn,center);
        case 'constraintsOneRxn'
            sens=sensitivityWithSoftConstraintsFormer(model,rxn(1),center);
    end
    res.labels=biomlabels;
    res.sens=sens.x;
    res.biomidx=biomidx;
    [~,v]=outlierDetectionMAD(res.sens,3);
    biomark=res.sens(biomidx);
    res.center=center(biomidx);
end

end
