
function [biomark,sens]=predictBiomarkersSensWithConstraintsGenes(model,genes,...
    center,biomidx,biomlabels,version)
% genes structure 
ng=size(genes,1);
nbiom=length(biomidx);
biomark=nan(ng,nbiom);
sens=zeros(length(model.rxns),ng);
for ig=1:ng
    ig
    [rxnTot, constrainRxn,~] = detectAffectedRxnsByGene(model,genes.Id(ig));
    rxn=rxnTot(constrainRxn);
    if ~ isempty(rxn)
        [bm,res]=predictBiomarkersSensWithConstraints(model,rxn, center,biomidx,biomlabels,version);
        biomark(ig,:)=bm;
        sens(:,ig)=res.sens;
    end
end
        
    
end