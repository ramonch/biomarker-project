function sensitivity = sensitivityWithConstraints(model, perturbedRxns, bounds)
% Performs a sensitivity version but taking into account constraints 
%
% USAGE:
%
%    sensitivity = sensitivityWithConstraints(model, perturbedRxns, FVAbounds)
%
% INPUTS:
%    model:          Wild type model
%    perturbedRxns :         Rxns to perturb
%    bounds: struct or matrix (if struct, the bounds are taken from FVA),
%    if not the bounds are a flux distribution
% 
% OUTPUTS:
%    sensitivity
% 
% .. Author: - Charlotte Ramon 

oSenseStr='min';
verbFlag=true;

if ~isstruct(bounds)
    ibounds.min=bounds;
    ibounds.max=bounds;
else
    ibounds=bounds;
end
% LP solution tolerance
global CBT_LP_PARAMS
if (exist('CBT_LP_PARAMS', 'var'))
    if isfield(CBT_LP_PARAMS, 'objTol')
        tol = CBT_LP_PARAMS.objTol;
    else
        tol = 1e-6;
    end
else
    tol = 1e-6;
end

[nMets,nRxns] = size(model.S);
nPert=length(perturbedRxns);

sensitivity.f = [];
sensitivity.x = [];
sensitivity.stat = -1;

selRxn=zeros(nPert,nRxns);
for i=1:nPert
    selRxn(i,perturbedRxns(i))=1;
end

% should we apply the same perturbation for all reactions by a single gene
if isstruct(bounds)
    perturbation=-(ibounds.max(perturbedRxns)-ibounds.min(perturbedRxns))
else
    % applies the perturbation so that the perturbation results in a zero flux 
    perturbation=-(ibounds.max(perturbedRxns));
    
    % applies a perturbation so that all the fluxes get the same
    % perturbation
    %perturbation=-sign(ibounds.max(perturbedRxns)).*repmat(ibounds.max(perturbedRxns(1)),nPert,1)
end

perturbation
        % Construct the LHS matrix
        % Rows:
        % 1: S*d = 0 
        % 2: dk = (FVAbounds.max(k)-FVAbounds.min(k))/2
        QPproblem.A=[model.S;...
                    selRxn];
        
        % Construct the RHS vector
        QPproblem.b=[model.b;...
                    perturbation];
                    
        
        % Linear objective = 0
        QPproblem.c = zeros(nRxns,1);

        % Construct the ub/lb
        %4:(lb): d>=lb-max(FVA)
        %5:(ub):  d<=ub-min(FVA)
        QPproblem.lb = model.lb-ibounds.max;
        QPproblem.ub = model.ub-ibounds.min;

        % Construct the constraint direction vector (G for delta's, E for
        % everything else)
        
        QPproblem.csense = repmat('E',nMets+nPert,1);        
        

        % F matrix
        QPproblem.F =  2*eye(nRxns);

    
    % in either case: minimize the distance
    QPproblem.osense = 1;
    
    if (verbFlag)
        fprintf('Solving Sensitivity with constraints: %d constraints %d variables ',size(QPproblem.A,1),size(QPproblem.A,2));
    end

    % Solve the linearMOMA problem    
    %QPsolution = solveCobraQP(QPproblem,[],verbFlag-1);
    QPsolution = solveCobraQP(QPproblem, 'printLevel', verbFlag-1, 'method', 0);

    if (verbFlag)
        fprintf('%f seconds\n',QPsolution.time);
    end

    % Get the solution(s)
    if QPsolution.stat == 1
        sensitivity.x = QPsolution.full(1:nRxns);
    end
    solStatus = QPsolution.stat;
    sensitivity.stat=solStatus;
    sensitivity.f=QPsolution.obj;
    sensitivity.solver = QPsolution.solver;
    sensitivity.time = QPsolution.time;

end
